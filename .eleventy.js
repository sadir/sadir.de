const cleanCss = require("clean-css");
const htmlmin = require("html-minifier");
const hyphenate = require("hyphen/de").hyphenateHTMLSync;
const imageSize = require("image-size");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const sitemap = require("@quasibit/eleventy-plugin-sitemap");
const fs = require('fs');

module.exports = function (config) {
    // config.addPassthroughCopy({ "favicons": "." });

    config.addCollection("dateien", function () {
        const files = [];
        const path = "../files.sadir.de";
        fs.readdirSync(path).map(fileName => {
            if (fileName.indexOf(".") > -1) {
                const extension = fileName.split(".")[1];
                const thumb = extension === "jpg" || extension === "png" || extension === "gif" ? fileName : `${fileName.split(".")[0]}.jpg`;
                const dimensions = imageSize(path + "/thumbs/" + thumb);
                files.push([fileName, thumb, dimensions.width, dimensions.height]);
            }
        });
        return files;
    });

    config.addCollection("links", function () {
        return JSON.parse(fs.readFileSync("links/links.json", "utf8"));
    });

    config.addCollection("posts", function (collection) {
        return collection.getFilteredByGlob("posts/*.md");
    });

    config.addFilter("cssmin", function (code) {
        return new cleanCss({}).minify(code).styles;
    });

    config.addFilter("ddmm", function (value) {
        const date = new Date(value);
        return `${("0" + date.getDate()).slice(-2)}.${("0" + (date.getMonth() + 1)).slice(-2)}.`;
    });

    config.addFilter("ddmmyyyy", function (value) {
        const date = new Date(value);
        return `${config.getFilter("ddmm")(value)}${date.getFullYear()}`;
    });

    config.addFilter("groupByMonth", function (collection) {
        const getDate = (element) => collection[0].length === 5 ? element[3] : element.data.date;
        const groups = collection.reduce((acc, element) => {
            const date = getDate(element);
            const monthyyyy = date === "Bis Juli 2021" ? date : config.getFilter("monthyyyy")(date);
            acc[monthyyyy] = acc[monthyyyy] || [];
            acc[monthyyyy].push(element);
            return acc;
        }, {});
        const pairs = [];
        for (const group in groups) {
            if (groups.hasOwnProperty(group)) {
                pairs.push([group, groups[group]]);
            }
        }
        return pairs;
    });

    config.addFilter("hostname", function (url) {
        return new URL(url).hostname;
    });

    config.addFilter("hyphenate", function (text) {
        return hyphenate(text, { hyphenChar: "&shy;", minWordLength: 10 });
    });

    config.addFilter("monthyyyy", function (value) {
        const date = new Date(value);
        const month = date.toLocaleString('de-DE', { month: 'long' });
        const year = date.getFullYear();
        return `${month} ${year}`;
    });

    config.addPlugin(pluginRss);

    config.addPlugin(sitemap, {
        sitemap: {
            hostname: "https://www.sadir.me",
        }
    });

    config.addTransform("htmlmin", function (content, outputPath) {
        if (outputPath.endsWith(".html")) {
            let minified = htmlmin.minify(content, {
                useShortDoctype: true,
                removeComments: true,
                collapseWhitespace: true
            });
            return minified;
        }
        return content;
    });
};