---
layout: post.njk
title: Hinter Trödelfabianen an der Kasse
date: 2022-12-29T12:15
dropCap: false
---
"Sammeln Sie Payback-Punkte?"

"Huh?"

Was "huh"? Dein erster Einkauf? JA oder NEIN?

"Wollen Sie den Beleg dazu haben?

"Ähmmm..."

Was "ähm"? Hör auf geistesabwesend zu sein und von Fragen überrascht zu werden, die du jedesmal bekommst. JA oder NEIN? Zack, boom, next! Ich hab nicht den ganzen Tag Zeit.