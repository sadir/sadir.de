---
layout: post.njk
title: Die Qualität eines Buches entsteht durch Reifung
date: 2020-11-11T07:45
---
Begeisterung wird häufig als Gütesiegel betrachtet. Wie oft liest nicht jemand ein Buch und denkt sich "Das ist das beste Buch, das ich je gelesen habe!" und wie oft wird danach noch an das Buch gedacht? Wieviele Bücher beeinflussen einen wirklich langfristig und wieviele würde man in fünf Jahren noch als must read weiterempfehlen? Wie gut etwas ist kann *nur* mit der Zeit gesagt werden, es ist nicht möglich sofort einzuschätzen welche Rolle dieses Buch noch im eigenen Leben spielen wird. Oft sind Bücher entweder Türöffner für noch bessere Bücher oder sie bestehen den Praxistest nicht und die Realität trübt das Bild. In jedem Fall sollte man auch nicht so viel auf Empfehlungen geben, wenn jemand etwas nur erst kürzlich gelesen hat.