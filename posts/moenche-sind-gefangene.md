---
layout: post.njk
title: Mönche sind Gefangene
date: 2022-12-31T15:20
---
Was ist, was einem als erstes in den Sinn kommt, wenn man über Häftlinge nachdenkt? Leute in gleichfarbigen Uniformen, allen bleiben Dinge verwehrt, die außerhalb der Gemäuer selbstverständlich sind, alle müssen sich an einen gewissen Tagesrhythmus halten, alle bekommen nur das Mindeste.

Noch einen Schritt geht es weiter, wenn man an Kriegsgefangene oder Häftlingen in Arbeitslagern denkt: alle werden kahlgeschoren. Ihnen wird so viel "individuelles" genommen wie möglich.

> **Zwischen den Soldaten konnte man in den Läden und Gängen Männer bemerken, die an ihren grauen Röcken und rasierten Köpfen als Sträflinge kenntlich waren.**  
> – Lew Tolstoi, Krieg und Frieden

Wüsste man nicht, dass es hier um Gefängnisse geht, könnte man es mit Klöstern verwechseln. Nur siehst sonst niemand Mönche als Gefangene, denn sie verbüßen keine Strafe, niemand hält sie da, denn sie selbst. Doch was auch immer jemanden dazu bewegt, sich in diese Situation zu begeben, ändert nichts an dem Resultat. Bei dem einen wird das Gefängnis zur Realität durch äußeren Zwang, bei dem anderen wird der innere Zwang zur Realität durch das Gedankengefängnis im Kopf.

Schließlich blutet ein Körper, ganz gleich, ob er durch Missbrauch oder Selbstkasteiung geschändet wurde. Der Körper nimmt nicht durch magische Weise weniger Schaden oder heilt schneller, bloß weil es freiwillig geschieht, beides sind Verletzte.