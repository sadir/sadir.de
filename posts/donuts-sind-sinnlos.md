---
layout: post.njk
title: Donuts sind sinnlos
date: 2023-01-01T00:45
---
Der Titel bringt es schon auf den Punkt. Es gibt wohl kein Gebäck, das so wenig Existenzberechtigung hat, wie Donuts. Donuts sind so etwas wie missglückte Berliner. Denn das beste an Berlinern ist die Füllung, der Rest ist sinnlose Teigmasse oder herunterschneiender Puderzucker bzw. eklig-klebrige Glasur. Donuts sind das gleiche, nur ohne das beste daran - die Füllung. Nicht nur ist das beste nicht vorhanden, also es ist nicht einfach Teig ohne Füllung in der Mitte, sondern da, wo eigentlich die Füllung sein müsste, ist einfach ein Loch, ein Nichts. Die komplette Leere, um noch einmal zu unterstreichen *wie sehr* das Beste daran fehlt. So bleibt also nur die sinnlose Teigmasse und die eklig-klebrige Glasur übrig.

Ehrlich gesagt, wäre ich Bäcker und ich wäre auf die Idee von Donuts gekommen, so hätte ich sie direkt verworfen, da ich nicht glauben würde, dass jemand so dumm ist, um das zu kaufen.