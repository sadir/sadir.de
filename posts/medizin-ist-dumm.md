---
layout: post.njk
title: Medizin ist dumm
date: 2020-08-21
---
Ich bin schon ganz früh nicht gerne zum Arzt gegangen und habe versucht es zu vermeiden, wo es nur geht. Wenn ich schließlich doch mal hingegangen bin und der Arzt mir etwas verschrieben hat, habe ich es nicht genommen oder nicht so lange, wie vorgeschrieben. Es war aber nicht die Angst vor Spritzen o.ä., weswegen ich ungern hingegangen bin – damit hatte ich nie ein Problem – es war eher ein tiefes Misstrauen. Ich konnte es damals auch nicht wirklich erklären, sodass andere es hätten nachvollziehen können; ich hatte einfach selbst tief in mir das Gefühl, dass es nicht normal ist. Dass irgendwas nicht mit rechten Dingen zugeht.

Erst viel später konnte ich das Problem auch benennen. Das Problem ist, dass die Medizin (und mit Medizin meine ich Schulmedizin/Allopathie, aber auch andere, wie Homöopathie) auf den Grundannahmen aufbauen, dass a) Ursachen bei der Krankheitsbekämpfung irrelevant sind und b) wir schlauer sind als unser Körper. Beide halte ich für extrem falsch und gefährlich.

Wenn jemand eine körperliche Beschwerde hat, dann hat es immer eine Ursache. Wenn jemand bspw. etwas Ungesundes zu sich nimmt und so seinen Körper intoxikiert, dann soll der Körper eine Möglichkeit haben, zu sagen, dass es falsch war. Wie sollen wir denn sonst erfahren, dass es nicht gut für uns war, wenn wir kein Signal zurückbekommen? Sind wir energielos, haben Kopfschmerzen oder einen anderen üblen Nebeneffekt, dann sollten wir aufmerksam sein. Unser erster Gedanke sollte nicht sein, dass die Schmerzen verschwinden sollen, sondern wir sollten uns fragen, woher sie kommen. Und ein Arzt ist leider keine Hilfe dabei, diese Frage zu beantworten.

Gehe ich mit einem Problem zum Arzt, dann erwarte ich, dass er seine Expertise nutzt, um mir ein Katalog an Fragen zu stellen und so per Ausschlussverfahren die mögliche Ursache herausfiltert. Er sollte mir sagen: "Das ist höchswahrscheinlich der Auslöser. Lass das mal die nächste Zeit sein und wenn das Problem in zwei Wochen immer noch nicht weg ist, dann komm wieder. Ich hoffe, ich sehe dich nicht wieder – und tschüss". Das ist für mich der optimale Ablauf beim Arzt.  
Stattdessen fragt er nur sowas wie "Wie lange hast du es denn schon?" und "Was hast du schon getan?" und blablabla und zückt am Ende seinen Stift und verschreibt mir irgendeinen Scheiß, nachdem ich nicht gefragt habe. Ich bin einmal wegen einem Schnupfen, der nach meinem Geschmack zu lange anhielt, zum Arzt gegangen und er hat mir nach etlichen Fragen dann Antibiotika verschrieben. Wo ich mich dann wundere, ob diese Fragen überhaupt einen Sinn hatten und es am Ende nicht so oder so darauf hinausgelaufen wäre.

> ![Robert Mendelson, M.D.](https://files.sadir.me/posts/20200820-robert-mendelson.jpg)
> **Despite the tendency of doctors to call modern medicine an 'inexact science', it is more accurate to say there is practically no science in modern medicine at all. Almost everything doctors do is based on a conjecture, a guess, a clinical impression, a whim, a hope, a wish, an opinion or a belief. In short, everything they do is based on anything but solid scientific evidence. Thus, medicine is not a science at all, but a belief system. Beliefs are held by every religion, including the Religion of Modern Medicine.**  
> – Robert Mendelson, M. D.

Ich möchte nicht etwas menschengemachtes, im Labor hergestelltes zu mir nehmen, um Symptome zu unterdrücken. Symptome sind gut. Wenn ich sie unterdrücke, aber die Ursache, die zu ihnen geführt hat, immer noch da ist, dann ändert es nichts am Zustand meiner Gesundheit: mein Organismus leidet, nur merke ich nichts davon. Die Konsequenz daraus wird sein, dass es zu einem noch größeren Problem führen wird und die Symptome, die dieses Problem begleiten werden, werden noch unerfreulicher sein. Man schiebt so Dinge nur auf die lange Bank und quält damit seinen eigenen Körper.

Auch die Hybris zu glauben, dass man eine Krankheit besser heilen könnte, als der Körper selbst, der sich in Millionen von Jahren genau um diese Aufgabe herum entwickelt hat, ist eine der Makel der heutigen Medizin. Zu glauben, der Körper könnte nicht mit gewissen Gegebenheiten, mit denen er schon immer zu tun hatte, umgehen, ohne irgendwelche Laborerzeugnisse, die erst paar Jahre aufm Markt sind, ist so absurd, wenn man sich vergegenwärtigt, dass das tatsächlich eine der Säulen ist, auf die die moderne Medizin in der Praxis aufgebaut ist.

So hart es auch klingen mag, aber Ärzte sind heutzutage nichts weiter als die Dealer der Pharmaindustrie. Im Zentrum der Medizin steht der Verkauf von chemischen Produkten und der Profit von den Herstellern ebendieser. Die Ärzte sind die Händler, die Mittelmänner zwischen Produzenten und Konsumenten. Und das ist nicht nur irgendeine pessimistische Interpretation der Lage, nein, das ist die ehrlichste und wahrste Wiedergabe unseres Systems.

> ![Linus Pauling](https://files.sadir.me/posts/20200820-linus-pauling.jpg)
> **Everyone should know that most cancer research is largely a fraud, and that the major cancer research organizations are derelict in their duties to the people who support them.**  
> – Linus Pauling

Was im Medizinstudium gelehrt wird und was in den Lehrbüchern steht, wird von der Pharmaindustrie kontrolliert. Was die aktuelle medizinische Forschung ist und was in den renommierten Magazinen gedruckt wird, wird ebenfalls von der Pharmaindustrie kontrolliert. Wo das Geld ist, ist die Macht.

Die akademische Forschung ist sehr langsam und bis gewisse Ergebnisse überhaupt in den Lehrbüchern der Studenten landen, vergehen viele viele Jahre. Wenn es sich um Ergebnisse handelt, die Profitverlust für die großen Unternehmen bedeuten würde, dann wird die Veröffentlichung von der Industrie sogar bewusst zurückgehalten und erschwert, sodass teilweise auch viele Jahrzehne ins Land ziehen können, bis sie an die breite Öffentlichkeit dringen.

Und als wäre das alles nicht schlimm genug, kommt die Tatsache hinzu, dass der Großteil medizinischer Studien schlichtweg falsch ist. Und mit Großteil meine ich tatsächlich **fast alle**. Professor John Ioannidis belegte genau das in seinem 2005 veröffentlichten Essay ["Why Most Published Research Findings Are False"](https://upload.wikimedia.org/wikipedia/commons/8/8e/Ioannidis_%282005%29_Why_Most_Published_Research_Findings_Are_False.pdf).

Wenn man sich das alles vor Augen führt, dass die meisten Forschungsergebnisse, die zum Konsens werden, nur diesen Status erreichen, weil es zum Vorteil für die Hersteller von Pharmazeutika ist; dass Ärzte Menschen sind mit lauter Falschinformationen und falschen Vorstellungen über den Körper im Kopf; dass zu Ärzten, die selbst vor 30-40 Jahren studiert haben, noch immer nicht gewisse Dinge durchgedrungen sind, die schon in ihrer Studienzeit als sicher feststanden, dann kann man nur zu einem Schluss kommen: dass wer einen Arzt aufsucht, sich in Teufels Küche begibt.

> ![Aajonus Vonderplanitz](https://files.sadir.me/posts/20200821-aajonus-vonderplanitz.jpg)
> **Anything the medical profession says, do the opposite 99 percent of the time and you'll be right.**  
> – Aajonus Vonderplanitz

Es ist auch schon lange kein Geheimnis mehr, dass viele Leute, die im Krankenhaus oder durch ärztliche Behandlung gestorben sind, nicht **trotz**, sondern **wegen** der Behandlung gestorben sind und noch am Leben wären, wenn sie sich niemals einem Arzt anvertraut hätten. Das nennt man Iatrogenik. Viele Menschen haben niemals davon gehört, obwohl die Zahlen erschreckend hoch sind. Ich vermute aber, dass die wahren Zahlen sogar noch höher sind als die offiziellen.

Der menschliche Körper ist sehr komplex, Laborexperimente verraten uns absolut gar nichts darüber, wie sicher oder unsicher ein Medikament für uns ist. Der Körper richtet sich nicht nach unseren Bedingungen. Als 1945 die Atombomben auf Japan abgeworfen wurden und die Menschen in der Umgebung dieser ständigen Strahlung ausgesetzt waren, sind erst 40 Jahre später (!) Fälle von Gehirntumoren aufgetreten. Nach der Logik von Laborforschungen würde man wahrscheinlich schon nach den ersten paar Jahren sagen, dass es keinen Zusammenhang zwischen der Strahlung und Gehirnschäden gibt. Wir haben absolut keine Ahnung, was diese Medikamente, diese Supplements, die Impfungen usw. langfristig in unserem Körper anrichten. Und wir sollten ihnen niemals niemals vertrauen.

<!--
> **Do not let either the medical authorities or the politicians mislead you. Find out what the facts are, and make your own decisions about how to live a happy life and how to work for a better world.**  
> – Linus Pauling
-->