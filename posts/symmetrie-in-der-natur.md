---
layout: post.njk
title: Symmetrie in der Natur
date: 2020-07-31
---
Ein Objekt ist für Menschen symmetrisch, wenn man es in der Mitte spiegelt und das Spiegelbild die gleiche Form hat wie die tatsächliche andere Hälfte – anders gesagt: wenn beide Seiten eines Objekts kongruent sind. Wenn man jedoch in die Natur guckt, dann kann man so etwas nirgendwo beobachten. Das Konzept von Symmetrie in Form von Deckungsgleichheit beider Seiten ist eine menschliche Erfindung.

Für die Natur ist Symmetrie ein Synonym für Gleichgewicht. Beispielsweise sind Bäume symmetrisch, da sie im Gleichgewicht sind. Sie sind auf der einen Seite nicht exakt gleich, wie auf der anderen Seite – all die Stämme, Zweige und Blätter unterscheiden sich – aber keine Seite schert so weit aus, dass er umkippt. Ebenso sind auch alle anderen Pflanzen und auch Berge und Meere usw. symmetrisch.

Eine planmäßige Symmentrie ist ein Irrtum. Ein Kreis, der mit einem Zirkel auf ein Blatt gezeichnet wird, ist ein Fantasiegebilde. Er ist auf die gleiche Weise Fantasie, wie eine Geschichte über Zauberer, Trolle und Elfen, die man auf das Blatt schreibt. Dem ist so, weil von Gleichgewicht kann man erst reden, wenn tatsächlich Kräfte darauf wirken und das Objekt dann einen Punkt findet, in der sich alle Kräfte mehr oder weniger ausgleichen. Das ist dann der Zustand der Symmetrie. Dieser Zustand ist aber ständiger Veränderung unterworfen, dadurch dass es immer wieder verändernden Kräften ausgesetzt wird. Ein und dasselbe Objekt kann also unzählige Varianten der Symmetrie haben. Und jede spezifische Symmetrie ist zu einem gewissen Grad dem Zufall unterlegen.

Ordnung, Struktur, Symmetrie, Gleichgewicht usw. **ohne** Zufall ist daher unnatürlich. Wenn man beachtet, dass diese Art Ordnung der Ursprung des Menschen und des Lebens an sich ist und sie unserem Sein zugrunde liegt, müssen Menschen, die eher die künstliche, geplante, zufallsfreie Ordnung der natürlichen Ordnung vorziehen, eine tiefsitzende und konditionierte Störung der Psyche haben. 