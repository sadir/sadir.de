---
layout: post.njk
title: Geld macht weniger materiell
date: 2021-07-31T16:40
---
Geld ist gleichbedeutend mit Freiheit. Es befreit einen von Zwängen und von Materiellem. Je weniger jemand besitzt, desto mehr hält er an dem fest, was er hat und desto schwerer ist er getroffen, wenn er etwas davon verliert. Wenn jemand bspw. durch einen Hausbrand all sein Hab und Gut verliert, wen wirft es mehr aus der Bahn, den Armen oder den Reichen? Der Reiche kommt sehr viel schneller wieder auf die Beine, da er noch genug Geld auf dem Konto hat. Er kann leichter Dinge ersetzten, während der Arme eine stärkere innere Verbundenheit zu seinem Besitz hat. Geld gibt einem die Freiheit über dem Materiellen zu stehen und nimmt einem die Sorgen darüber.