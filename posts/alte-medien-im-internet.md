---
layout: post.njk
title: Alte Medien im Internet
date: 2022-01-18T13:45
---
Es gibt viele Menschen, die nutzen das Internet fast ausschließlich nur dafür Medien zu konsumieren, die es bereits vor dem Internet gab. Also Nachrichten großer Medienhäuer, Filme von bekannten Filmstudios usw.

In den Anfangsjahren des Internets gab es den Traum, dass es die Welt in irgendeiner Weise verändern könnte, da wir jetzt Stimmen hören können, die es nicht in den Fernsehen geschafft hätten. Stattdessen hat es die Welt nur in der Hinsicht verändert, dass es alle zu noch größeren Konsumenten gemacht hat und sie noch abgelenkter und unproduktiver wurden. Das Internet hat alles beim Alten gelassen, das Alte aber noch extremer gemacht.