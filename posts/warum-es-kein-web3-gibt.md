---
layout: post.njk
title: Warum es kein Web 3.0 gibt
date: 2022-04-29T23:00
---
Es gibt einen guten Grund, wieso Web3 im Gegensatz zu Web2 nur ein Buzzword ist, kreiert und verbreitet von Geschäftsleuten in der Kryptobranche, um naiven Leuten eine weltverändernde, neuartige Technologie zu verkaufen. Nun, ist die Technologie darunter tatsächlich neu und anders als unser bisheriges zentralisiertes Modell? Ja. Ergibt es einen Sinn die Versionsnummer des Webs deswegen zu inkrementieren? Nein.

Der Grund, wieso man das Internet ab ungefähr 2005 überhaupt Web 2.0 genannt hat, war, dass sich der Umgang mit ihm verändert hat. Während es davor hauptsächlich aus statischen Seiten bestand, die man besuchte, um sie zu konsumieren, konnte man als Nutzer plötzlich selber den Inhalt der Seite verändern. Man konnte auch problemlos und ohne technische Kenntnisse selber seine eignen Räume im Internet schaffen und eigene Inhalte veröffentlichen. 

Und seit der Verbreitung von Smartphones konnte man es auch zusätzlich immer und von überall tun. Das ist eine klare Veränderung wie man mit dem Medium umgeht und hat daher auch den Namen Web 2.0 verdient.

Wenn wir jetzt aber Dienste im Internet anfangen zu dezentralisieren, funktioniert die darunterliegende Technologie aus der Sicht der Entwickler vielleicht anders, aber aus der Sicht des Nutzers fängt man nicht an seinen Umgang mit den Seiten zu verändern. Nach wie vor sind sie dynamisch, nach wie vor kann man eigene Inhalte posten. Man merkt noch nicht einmal, ob man auf einer Web2- oder Web3-Seite ist, wenn es einem nicht gesagt wird.

Wenn wir die Definition der Versionsnummer insoweit verändern, dass sie jetzt nicht nur die User Experience beinhaltet, sondern auch die der Programmierer, dann wären wir jetzt viel weiter als 3.0, denn es gab im Laufe der Internetgeschichte mehrere Entwicklungen, die man als mehr oder wenig große Durchbrüche bezeichnen konnte, bspw. die Entstehung von PHP, die Entstehung von JavaScript und Ajax, der Trend hin zu Single Page Applications, die Verbreitung von Serverless und Cloud Computing. All diese Entwicklungen sind andere Ansätze wie man seine Seiten entwickeln kann, dennoch verlangt keiner, dass wir eine neue Versionsnummer für das Web brauchen.

Es gibt aber eine andere Entwicklung in den letzten ca. 10 Jahren, die man tatsächlich als Web 3.0 bezeichnen könnte. Nämlich Stories in Sozialen Medien. Generell das Konzept, dass man Dinge veröffentlicht mit der Absicht, dass sie bald wieder vernichtet werden. Apps wie Clubhouse fallen genau in diese Sparte. Sie versuchen die Erfahrung im echten Leben möglichst nachzubildern – wer live dabei oder schnell genug war, bekommt den Inhalt zu sehen und danach existiert das Gesehene nur noch in der Erinnerung der Zeugen. So wie auch das Erlebte im echten Leben. Das ist ein klarer Einschnitt in den traditionellen Postings und man könnte es als eine neue Art bezeichnen, wie man mit dem Web umgeht.