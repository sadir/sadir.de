---
layout: post.njk
title: Essen mit Händen
date: 2020-08-07
---
Dass es bei Essen nicht nur auf den Geschmack ankommt, ist uns schon lange bekannt. Sprichwörter wie "Das Auge isst mit" oder auch die Tatsache, dass man sich die Nase zuhält, wenn man versucht etwas Ungenießbares herunterzubekommen, belegen, dass wir Essen durch viele Sinne aufnehmen, nicht nur durch den Geschmackssinn. Ich behaupte aber, dass höchstwahrscheinlich alle Sinne dabei eine Rolle spielen. Vor allem ein Sinn, der in dem Zusammenhang kaum genannt wird: die Haptik.

Wir haben viele Millionen Jahre ausschließlich mit unseren Händen gegessen. Messer und Gabel als Erweiterung unseres Körpers gab es die meiste Zeit in unserer Entwicklungsgeschichte gar nicht. Essen bedeutete das Essen zu berühren – es war ein integraler Bestandteil der Erfahrung der Nahrungsaufnahme.

Heute, wo es zu den guten Manieren gehört, dass man zu Hause Besteck benutzt – also von seiner Nahrung getrennt wird – ist Fast Food beliebter denn je. An jeder Ecke gibt es Läden, die einem Essen auf die Hand geben. Die meisten Menschen denken, die Beliebtheit kommt vom Geschmack oder davon, dass es schnell geht. Aber ich denke, Geschwindigkeit spielt eine untergeordenete Rolle. Oft wollen sich die Leute hinsetzen und in aller Ruhe essen. Es geht viel mehr darum, dass es hier gesellschaftlich konform ist, dass sie ihre Nahrung ertasten und mit den Händen zu Mund führen können.

Fast Food ist eine gute Demonstration davon, wie sehr uns unsere Urinstinkte immer noch lenken. Ich glaube, die Beliebtheit davon würde rapide abnehmen, wenn es plötzlich hieße, es wäre unzivilisiert oder zurückgeblieben, wenn man es ohne Besteck isst. Oder wenn man umgekehrt Tischmanieren allgemein abschafft und es als völlig normal etabliert, dass überall mit den Händen gegessen wird.

Die Beliebtheit von Fast Food ist also sehr wahrscheinlich nichts weiter als eine Kompensation für die Verbreitung von Tischmanieren. Nun ist das natürlich nicht leicht zu beweisen. Man könnte sicher gewisse Settings für wissenschaftliche Untersuchungen schaffen, passende Methoden anwenden und die Ergebnisse dann auswerten. Wie aussagekräftig es dann wäre, weiß ich nicht. Am erfolgreichsten wäre so ein Test aber wahrscheinlich in einer dystopischen Welt, in der der Staatsapparat sich mit der Zeit verändernde Gebote und Verbote vorgibt und schaut, in welche Richtung es sich entwickelt.