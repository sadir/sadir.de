---
layout: story.njk
title: Fragen an Gott
date: 2022-08-11
story: true
---
Werner konnte es nicht glauben. Das Gerät, um mit Gott zu kommunizieren war endlich fertig. Er hatte 19 Jahre lang daran gearbeitet, er, der hellste Geist seiner Zeit mit seinem Projekt, an das niemand geglaubt hatte.

Er steckte die letzten Kabel ein und drückte einige Knöpfe.

»Hier spricht der Herr.«

Die tiefe, aber angenehme Stimme kam aus dem Lautsprecher und erfüllte den ganzen Raum. Werner blieb kurz die Luft weg. Instinktiv fiel er in einen Stuhl zurück und griff nach seinem Sohn Jonathan, der neben ihm saß. Der 16-jährige hatte sich bis jetzt immer nur über dieses absurde Projekt seines Vaters lustig gemacht. Seit er denken konnte hatte dieser daran gearbeitet.

Bevor sich Werner noch einmal fassen konnte, sprach Gott schon weiter: »Für diese außerordentliche Leistung, Werner, gebe ich dir eine Belohnung. Du darfst drei Fragen stellen, Dinge, die du immer wissen wolltest und ich werde sie dir beantworten.«

»Ich-ich weiß nicht was ich sagen soll. Das ist so gütig, Herr«, stammelte der tiefreligiöse Werner.

Er schaute zu seinem Sohn und sagte: »Er soll die erste Frage haben... Sohn, frag den Herrn was du schon immer wissen wolltest.«

»Ich? Ähm...«. Jonathan war sichtlich überfordert, »Man kann echt alle Fragen stellen?«

»Fast«, sprach Gott, »Es gibt zwei Fragen, die kann ich nicht beantworten, weil dieses Wissen in den Händen Normalsterblicher die Integrität des Universums und die Natur der Existenz gefährden würde.«

»Um welche Fragen handelt es sich, Herr«, fragte Werner, dessen Neugier jetzt geweckt war.

»Auch das werde ich nicht beantworten«, antwortete Gott, »Ihr werdet es erfahren, wenn ihr zufällig eine der Fragen stellt.«

»Okay, meine erste Frage ist«, sagte Jonathan, nachdem er kurz überlegt hatte, »Wieviele Spatzen gibt es auf der Welt?«

»Was?!« Werner schaute seinen Sohn ungläubig und mit offenen Mund an.

»Äh... Das wollte ich schon immer wissen. Egal, wo ich hingehe, da sind Spatzen. Und die sehen alle so gleich aus. Ich frage mich, wieviele gibt es bitte von denen? Oder sind es immer die gleichen?«

Jonathan, sich keiner Schuld bewusst, blickte nur seinen Vater verteidigend an. Dieser wendete sich aber wieder an das Gerät und sagte: »Herr, du musst uns entschuldigen. Mein Sohn, er ist manchmal...«

»Es ist in Ordnung«, unterbrach ihn Gott, »Alle Fragen können gestellt werden.«

Es folgte eine kurze Pause. Dann fuhr Gott fort: »Doch ich muss leider bedauern, dass ich dies nicht beantworten kann. Das ist einer der zwei Dinge, die ihr nicht wissen könnt.«

»Wir können nicht wissen, wieviele Spatzen es gibt, weil das sonst die Integrität des Universums gefährdet?« Jonathan wusste nicht, ob das ein Scherz sein sollte. »Gott, ich verstehe nicht...«

»Ich weiß«, sagte Gott, »Ihr werdet nicht in der Lage sein es zu verstehen, selbst wenn ich den Grund nenne, da euer Verstand nicht dafür gemacht ist. Was ist eure zweite Frage?«

»Die übernehme ich diesmal«, sagte Werner, »Herr, was ist der Sinn des Lebens?«

Wieder folgte eine kurze Pause.

»Der Sinn des Lebens«, sprach Gott, »Besteht darin, Spatzen ein möglichst einfaches Leben zu ermöglichen, indem man sie füttert und ihre Fressfeinde tötet.«

»I-ihre Fressfeinde töten? Spatzen? Sinn des Lebens?« Werner verstand die Welt nicht mehr.

»Marder, Habichte, Katzen«, sagte Gott, »Wenn auch immer ihr diese seht, besteht eure Aufgabe darin diese zu töten.«

»Wir sollen Katzen töten?!«

Plötzlich bewegte sich das Gerät kurz auf und ab. Werner war perplex, denn eigentlich hatte er dem Gerät keine Funktion zum Nicken eingebaut.

»Aber Herr, du hast diese Tiere doch erschaffen, wie können wir Wesen töten, die von unserem Gott kreiert wurden?«

»Ich habe Katzen ganz bestimmt nicht erschaffen. Die sind das Produkt von Satan.«

»Was?! Katzen hat der Teufel erschaffen? Aber-«

»Werner«, sprach Gott, »Du gehörst zu den klügsten Köpfen der Gegenwart. Wie kannst du das nicht sehen? Wie kann es noch offensichtlicher sein?«

»Herr, ich fass es nicht«, sagte Werner, »Ich hatte früher eine Katze. Ich habe sie geliebt.«

»Und wo ist sie jetzt?«, fragte Gott.

»Da-das weiß ich nicht. Sie ist vor vielen Jahren abgehauen. Ich würde alles dafür tun, um sie wiederzusehen...«

»Soll ich dir sagen, was mit ihr passiert ist?«, fragte Gott.

Werners Augen leuchtete kurz auf.

»Willst du, dass das deine dritte Frage wird?«, bot Gott an.

Jonathan konnte sehen, dass sein Vater kurz davor war zuzustimmen und griff nach seinem Arm.

»Nein, Papa! Halt dich zurück, bitte. Verschwende die Frage nicht für Mimi. Die ist schon so lange weg und- und die ist sowieso ein Produkt des Teufels gewesen, du hast es gerade gehört. Bitte, stell eine vernünftige Frage.«

»Aber Sohn«, sagte Werner, »Ich liebe Mimi so sehr. Und die Frage, was mit ihr passiert ist, hat mich so viele Jahre geplagt. Wenn ich es jetzt erfahre, kann mein Kopf endlich ruhen.«

Ohne eine Antwort abzuwarten, wendete Werner sich wieder an das Gerät und sagte: »Ja, das soll meine letzte Frage werden. Was ist mit meiner Katze passiert?«

»Sie ist zurück in die Hölle«, antwortete Gott.

Werner stockte erneut der Atem. Er wollte etwas sagen, aber brachte nur stammelnd einige Laute heraus.

»Was glaubst du wohin all die vermissten Katzen gehen«, fuhr Gott fort, »Der Teufel holt sie sich zurück.«

»Aber-aber, Herr, was sollen die denn in der Hölle? Was will der Teufel von ihnen?«

»Die werden als Folterknechte eingesetzt«, sagte Gott, »Der Teufel lässt sie die Menschen dort peinigen.«

»Meine Mimi... meine Mimi foltert Menschen in der Hölle...«, Werner standen Tränen in den Augen. Er konnte nicht aufhören mit dem Kopf zu schütteln.

»Nun gut«, sagte Gott, »Ich unterbreche hiermit die Kommunikation. Das Gerät wird sich von alleine zerstören und mit der selben Methode wird es ab jetzt nicht mehr möglich sein Kontakt mit mir aufzunehmen.«

»Herr, bitte«, Werner klang verzweifelt, »Was soll ich jetzt tun?«

»Ich weiß was du denkst«, sagte Gott, »Du kannst tun, was du willst. Nur wisse, Suizid ist eine Sünde.«

»Bitte?«

»Man kommt dafür in die Hölle.«

»Ich weiß. Aber warum sagst du mir das?«

»Weil ich deine Gedanken lesen kann«, sagte Gott, »Du hast daran gedacht deine Katze wiederzusehen.«

Und damit brach der Kontakt ab und mit einem kurzen Zischen und Biepen ging das Gerät kaputt.