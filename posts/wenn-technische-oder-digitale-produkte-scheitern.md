---
layout: post.njk
title: Wenn technische oder digitale Produkte scheitern
date: 2023-01-01T00:30
---
Wohl niemand wird von der Hand weisen, dass für Erfolg immer auch Glück eine Rolle spielt. Der Glücksfall zur richtigen Zeit am richtigen Ort zu sein. Dieses Glück ist aber nicht gleichwertig – denn dass ein gutes Produkt scheitert ist größer, als dass ein schlechtes Erfolg hat.

Doch was ist ein schlechtes Produkt? Ein schlechtes Produkt ist ein Produkt, das um seinetwegen gemacht wird.

Wenn eine Branche boomt, dann gibt es oft Leute, die in ihrem Enthusiasmus auch etwas verändern wollen. "Messenger, aber *anders*", "Finanzmanagementtool, aber *neu gedacht*". Diese Vorhaben werden aus dem Geltungsdrang des Entwicklers gemacht, aus keinem anderen Grund.

## Google Glass
Der iPhone hat den Umgang mit digitalen Inhalten und dem Internet für die große Masse der Welt grundlegend verändert – zumindest hat es einen wichtigen Grundstein dafür gelegt. So wie das Internet und der Computer Jahrzehnte zuvor unseren Umgang mit Informationen und unsere Kommunikation verändert haben.

Diese Innovationen erschaffen eine Kultur, die Leute anzieht, die ebenfalls Teil davon sein wollen. Die den Grundstein für *the next big thing* legen wollen. Das Resultat davon ist dann ein Produkt wie Google Glass.

Die Existenzberechtigung für Glass soll sein, dass wir mittlerweise so mit dem Internet verschmolzen sind, dass wir vorm Schlafengehen nicht sagen könnten, wie lange wir es benutzt haben; dass wir immer unseren Smartphone bei uns haben und es gebrauchen, wass immer uns danach ist, sodass es keine klaren Zeitfenster gibt, auf die man weisen kann, wenn es um die Internetnutzung geht, sondern dass im Internet sein parallel zum normalen Leben stattfindet. Wenn dem so ist, wieso braucht man dann ein Extragerät in der Hosentasche, das man immer herausholen muss? Wieso das ganze nicht immer vor Augen haben?

Der Denkfehler hier aber ist, dass das Herausholen aus der Tasche nie ein Problem dargestellt hat. Noch nie hat jemand gedacht "Ich würde gerne die Uhrzeit wissen, aber es ist eine viel zu große Hürde jetzt das Smartphone aus der Tasche zu holen, ich wünschte ich würde einfach auf ein Knopf an der Brille drücken, die ich immer auf der Nase habe." Und weil es kein Problem löst, weil niemand danach gefragt hat, so ist es auch zum Scheitern verurteilt.

## Smartphones
Um Smartphones ist es jedoch anders bestellt. Ich erinnere mich bei den alten Tastaturhandys wie schlecht bis unmöglich das Surfen auf Webseiten war, wie ich mir wünschte eine Möglichkeit zu haben, um schnell nach Informationen zu suchen. Ich erinnere mich auch, wie Leute damals darüber geredet haben, dass man in Zukunft ein WM-Spiel auf seinem Handy gucken können wird, während man gerade in der Bahn sitzt. Das waren Dinge, die Leute als Einschränkungen oder Probleme der alten Handys wahrgenommen haben, deswegen wurden Smartphones so schnell so erfolgreich, denn sie waren da, um diese zu lösen.

## Einwand 1: Pferde und Autos
Doch sagte nicht Henry Ford einmal: "If I would have asked people what they wanted, they would have said faster horses."? Autos lösen Probleme, die die Einschränkungen von Pferden mit sich bringen, wie also kann man glauben, dass Leute auf die richtige Lösung ihrer Probleme setzen würden?

Nun, wenn ich Leute frage, was sie wollen, ohne ihnen die Option "Auto" anzubieten und genau zu beschreiben, worum es sich dabei handelt, kann ich nicht erwarten, dass sie von alleine auf diese Erfindung kommen. Dass sie sich, nachdem es erfunden wurde, dafür entschieden haben, statt sich eine schnellere Pferderasse zuzulegen, impliziert, dass sie sich auch für Autos entscheiden würden, wenn man bei der Frage, was sie wollen, beide Optionen vorgelegt bekommen hätten, statt die Frage offen zu stellen; was diesen Einwand erübrigt.

## Einwand 2: Beme
Der amerikanische YouTuber Casey Neistat hatte die Social Media-App Beme herausgebracht, die sich dadurch auszeichnete, dass man Videos nur aufzeichnen konnte, wenn man das Smartphone an seine Brust hält und das Display während des Filmens nicht sehen konnte. Das hatte das Problem gelöst, dass Social Media infiziert ist von Selbstdarstellern, die penibel genau auf ihre Außenwahrnehmung achten mit Einsatz von Filtern, den besten Winkeln, den besten Momenten etc. Es würde also das Internet realer machen, es würde diese gekünstelte Scheinwelt zerstören. Warum hatte es dann nie Erfolg und wurde einige Jahre darauf eingestellt, obwohl es eine Sache löst, die viele Leute tatsächlich als Problem wahrnehmen?

Darauf gibt es zu sagen, dass es hier zwei Seiten gibt: User/Follower und Content Creator. Für die User ist das Angesprochene ein Problem, weil sie die künstlichen Inhalte konsumieren müssen – viele vergleichen ihr eigenes Leben damit, was auch zu Depressionen führen kann. Für die Creator aber ist das kein Problem. Ganz im Gegenteil, sie nehmen ja diesen Mehraufwand in Kauf, weil sie genau das *wollen*. Und die Creatoren sind es, die diese App hätte überzeugen müssen. Das konnte aber nie glücken, da dann eines der Hauptfeatures wegfallen würde, weswegen sie Social Media überhaupt benutzen: eine Idealwelt zu kreieren, die sie den Leuten verkaufen. Es war also zum Scheiern verurteilt.