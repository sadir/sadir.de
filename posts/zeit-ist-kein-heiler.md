---
layout: post.njk
title: Zeit ist kein Heiler
date: 2022-07-31T19:20
---
Leute mögen es zu sagen, dass Zeit alle Wunden heilt. Doch das ist so, als würde man sagen, dass Zeit alle Häuser baut. Denn zwischen dem Punkt, an dem jemand in meiner Nachbarschaft ein Grundstück kauft und dem Punkt, an dem dort ein fertiges Haus steht, ist lediglich Zeit vergangen. Also war Zeit verantwortlich für den Bau des Hauses.