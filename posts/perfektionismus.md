---
layout: post.njk
title: Perfektionismus
date: 2021-02-07T12:20
---
Ich will gar nicht wissen, wieviele potentiell großartige Projekte nie das Licht der Welt erblickt haben, weil ihre Macher niemals zufrieden genug waren, sie fertigzustellen. Perfektionismus hat in weiten Kreisen immer noch eine positive Konnotation, ist aber in Wirklichkeit eine Plage.

Sobald man anfängt die Fertigstellung eines Projektes auf die lange Bank zu schieben, weil sie nicht der Idealvorstellung in deinem Kopf entspricht, sollten direkt die ersten Alarmglocken läuten. Was auch immer das Ideal ist, frag dich, wenn du es tatsächlich genauso hinbekommst, wie du es dir vorstellst, wird es sich niemals ändern? In 2 Jahren? Vielleicht in 5 oder 10 Jahren, wird es immer gleich bleiben?

Die Antwort ist einfach, natürlich wird es sich ändern. Nun, wenn dem so ist, dann ist es kein Ideal. Wenn sich sowieo alles ändert, wenn sogar alles in der Natur ständiger Veränderung ausgesetzt ist, was genau glaubst du, strebst du da an?

Mehr als oft zeigt sich in der Praxis erst, dass man ein völlig verzerrtes Bild von der Realität im Kopf hatte. Erst wenn man es macht, wird sich ein echtes und realistisches Ideal herauskristallisieren.

> **Um zu wissen, was man zeichnen will, muss man zu zeichnen anfangen.**  
> – Pablo Picasso

Aber wie kämpft man dieses starke Gefühl an, dass es nicht gut genug ist, dass man wenigstens noch ein bisschen daran weiterarbeiten kann, damit es zumindest einermaßen deinen Vorstellungen entspricht? Ganz einfach, indem man sich Prioritäten setzt. Frag dich, was ist der allererste Sinn und Zweck des Projekts? Dann: Was wäre der einfachste und doch allgemein angesehnste Weg genau das umzusetzen? Und genau das machst du dann. Und wenn das Projekt erst einmal gestartet ist, kannst du es immer noch so ändern, wie du glaubst, dass es am besten ist.