---
layout: post.njk
title: Religion und Glaube
date: 2021-07-21T16:25
---
Menschen unterscheiden die beiden Begriffe "Religion" und "Glaube" oft so, dass sie Glaube als eine Art Grundspiritualität oder einen einfachen Glauben an etwas Höheres sehen und Religion als organisierten Glauben mit heiligen Schriften und Institutionen. Doch diese Definitionen sind nicht wirklich praktisch bzw. spiegeln nicht die Wirklichkeit wider. Viel besser wäre es, wenn Religion alles ist, was einem einen moralischen oder ethischen Kodex gibt, etwas, was das eigene Verhalten lenkt und Glaube ist, wenn diese Religion in einer heiligen Schrift, also einem blinden Glauben oder Vertrauen, fußt. Demnach würde es keinen unreligiösen Menschen geben, denn jeder hat einen inneren Kompass, was richtig und falsch ist, doch es würde ungläubige Menschen geben, denn nicht von jedem fußt der Kompass auf göttlichem Glauben.