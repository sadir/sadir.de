---
layout: post.njk
title: Richard David Precht ist langweilig
date: 2020-10-15T00:20
---
Precht ist das Paradebeispiel eines öffentlichen Denkers in Deutschland – kann gut reden, aber hat nichts spannendes zu sagen. Seine Eloquenz trägt so ziemlich sein ganzes Produkt, denn inhaltlich ist er sowas wie ein Pressesprecher für das System. Er tut nichts anderes als dem Staat nach dem Mund zu reden und wenn es mal Kritik gibt, ist es spezifische Regierungskritik. Seine Ansichten sind alle parteipolitisch einzuordnen und selbst wenn man etwas nicht in einem Parteiprogramm finden kann, gibt es innerhalb von Parteien doch viele Politiker, die das gleiche denken. Er ist für mich auch nicht wirklich ein Philosoph, eher ein Philsophielehrer. Er ist der Philosophieprofessor für die breite Öffentlichkeit. Und weil er diese Lehramtstelle behalten will, wird er alles dafür tun, um dem Arbeitgeber zu gefallen.