---
layout: post.njk
title: Theorie wurde zu Ideologie
date: 2021-09-22T11:20
---
In der Presse wird, wenn über Verschwörungstheorien geredet wird, in den letzten Jahren stattdessen immer der Begriff Verschwörungsideologie verwendet. Dabei ergibt diese Neubenennung nicht einmal Sinn. Denn eine Ideologie ist die eigene Meinung zu einem politischem oder religiösem Thema.

Denkt man bspw., dass die Ausgaben fürs Militär erhöht werden sollen, ist dass Teil einer Ideologie von jemandem. Reduziert der Verteidigungsminister aber die Ausgaben und man vermutet, dass der Grund, den er dafür angegeben hat, gelogen ist und er eigentlich andere Absichten hat, ist es eine Theorie. Ideologie geht also um die eigene Meinung, Theorie um die Meinung eines anderen.

Die Presse verwendet trotzdem den Begriff Verschwörungsideologie, weil das Wort "Theorie" viel zu positiv behaftet ist und viele Menschen es mit Wissenschaft assoziieren, während "Ideologie" immer eine Wolke von Gehirnwäsche über sich zu schweben hat. Daher verzichten die Medien wie so häufig auf Logik, nur um die Massen zu manipulieren und sie durch entsprechende suggestive Benennung in eine gewünschte Denkrichtung zu lenken.