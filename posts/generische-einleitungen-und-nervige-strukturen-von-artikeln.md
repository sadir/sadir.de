---
layout: post.njk
title: Generische Einleitungen und nervige Strukturen von Artikeln
date: 2020-10-15T00:40
---
Irgendwie ist es unter Journalisten, aber auch unter Videomachern zum guten Ton geworden, nicht direkt über das Thema zu reden, worum es geht, sondern erst einmal sinnlose Einleitungen zu verfassen. Geht es zum Beispiel um das Vermögen von Putin, dann muss natürlich erst einmal geschrieben werden wer Putin überhaupt ist, wie lange er schon Präsident ist und so weiter. Geht es um Falschmeldungen in Social Media, braucht es auf jeden Fall zu Beginn einen Absatz, in dem beschrieben wird, dass das Internet nicht mehr aus unserem Leben wegzudenken ist.

Aber nicht nur haben Artikel überflüssige Einleitungen, sondern auch nervige Strukturen, die nur darauf ausgelegt sind sich möglichst oft zu wiederholen. Eine Information muss immer erstens im Titel stehen, dann in der dick gedruckten Einleitung, dann in der Bildunterschrift und dann nochmal im normalen Text, am besten mind. zweimal. So dass man bei jedem Artikel fünf bis sechs mal das gleiche gelesen hat. Ich kann mir vorstellen, dass Journalisten diese Vorgabe bekommen und dass dahinter ein System steckt, aber es bleibt trotzdem nervig und so schreibt kein Mensch von sich aus, also lasst den Quatsch doch einfach sein.