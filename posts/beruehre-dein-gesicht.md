---
layout: post.njk
title: Berühre dein Gesicht!
date: 2020-08-02
---
Ich habe schon ganz früh bei mir selbst beobachtet, dass wenn ich Kopfschmerzen habe oder einen Druck über einem Auge oder in der Stirn spüre, dieses unangenehme Gefühl sofort um ein Vielfaches zurückgeht, wenn ich meine Hand an diese Stelle lege. Je mehr von der Handfläche ich auf die Stelle gelegt habe, desto beruhigender hat es gewirkt. Ebenso konnte ich beobachten, dass wenn mir jemand etwas kompliziertes erzählt, was meine volle Aufmerksamkeit erfordert, ich der Sache besser folgen konnte, wenn ich mein Kopf an der Handinnenfläche oder der Faust abstütze. Alternativ auch, wenn ich meinen Kinn berühre oder meine Wange, z.B. mit zwei oder mehr ausgestreckten Fingern oder meine Stirn usw.

![Personen, die denken](https://files.sadir.me/posts/20200731-denken-collage.png)

Auch die bekannte Skulptur "Der Denker" von Auguste Rodin ist im Prinzip nichts anderes, als die Darstellung eines Mannes, der zum konzentrierten Nachdenken sein Gesicht anfässt.

![Der Denker von Auguste Rodin](https://files.sadir.me/posts/20200731-der-denker.jpg)

Doch nicht nur zum Nachdenken neigen wir uns am Gesicht zu berühren, auch in Schocksituationen tun wir es oft ganz instinktiv, da es uns gewissermaßen beruhigt.

![Personen, die geschockt sind](https://files.sadir.me/posts/20200731-schock-collage.png)

Es gibt also einen klaren Zusammenhang zwischen unserem Tastsinn und unserer Psyche. Tatsächlich wurde dieser Effekt auch von Dr. Martin Grunwald von der Universität Leipzip et. al. wissenschaftlich untersucht:

* [EEG changes caused by spontaneous facial self-touch may represent emotion regulating processes and working memory maintenance](https://pubmed.ncbi.nlm.nih.gov/24530432/)
* [Self-touch: Contact durations and point of touch of spontaneous facial self-touches differ depending on cognitive and emotional load](https://pubmed.ncbi.nlm.nih.gov/30861049/)

Jetzt muss man sich vorstellen, es gibt jemanden, der nicht will, dass die Bevölkerung weit genug geistig zur Ruhe kommt oder dass sie Dingen nicht mit genug Aufmerksamkeit und Konzentration folgen kann, was würde er ihr wohl raten...