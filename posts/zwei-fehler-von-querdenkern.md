---
layout: post.njk
title: Zwei Fehler von Querdenkern
date: 2023-01-16T19:50
---
Für die breite Öffentlichkeit stehen Querdenker für ihre Antihaltung gegenüber den Coronamaßnahmen. Sie werden sogar so sehr damit assoziiert, dass *jeder*, der gegen die Maßnahmen ist, als Querdenker bezeichnet wird. Dies ist jedoch falsch, denn Querdenker sind nicht etwa eine generelle Gruppe, die eine Position unabhängig von Motiven vertritt, sondern sie zeichnen sich vor allem durch zwei Denkweisen aus. Dies sind die zwei Gründe, die mich dazu bringen, mich von ihnen zu distanzieren.

## 1. Konsequenz über Prämisse
Querdenker sind politisch motiviert. D.h. sie sind gegen die Maßnahmen, weil diese ihre Freiheitsrechte einschränken. Am Anfang steht aber immer ein Grund, der zu diesen Maßnahmen führt, also die Prämisse (Pandemie) löst die Konsequenz (Maskenpflicht, Lockdown etc.) aus. Querdenker beginnen jedoch am Ende dieser Kette und gehen rückwärts zum Anfang. Ihnen gefällt die Konsequenz nicht? Also stellen sie sich auch gegen die Prämisse. Sie glauben nicht, dass es eine Pandemie gibt und halten es für eine Verschwörung. Hat es einen fachlichen oder substanziellen Grund? Nein. Der einzige Grund ist, dass ihnen die Folge davon, wenn sie dem glaubten, nicht gefallen würde.

Dabei ist die Umkehrung von Prämisse und Konsequenz hier aber gar nicht nötig, denn ersteres hat weder Hand und Fuß, sodass alle Entscheidungen, die darauf basieren, sich erübrigen. Gäbe es wirklich eine hoch ansteckende und tödliche Pandemie, wäre es komplett kindisch und sogar höchst unvernünftig diese aus politisch-ideologischen Gründen zu ignorieren. Dann müsste man tun, was notwendig ist. In der Realität ist aber natürlich nichts notwendig, denn besagte Pandemie existiert nicht. Dass Querdenker hier die richtige Meinung haben, ist also eher ein Glücksgriff.

## 2. Der Glaube, dass die Gesellschaft dadurch zerstört wird
Wenn jemand gegen die Maskenpflicht ist und draußen lauter maskierte Gesichter sieht, wird er sich sehr unwohl fühlen, denn er hat das Gefühl, dass die Welt verdummt wurde. Diese Menschen haben aber schon vor 2020 existiert und verändert haben sie sich auch nicht. Um herauszufinden, dass sie dumm sind, hätte man sie aber erst kennenlernen müssen. Hat man sie nur im Vorbeigehen gesehen, konnte man ja nicht in ihre Gehirne schauen. Jetzt tragen sie aber ihre Gehirne vor der Nase. Was einem natürlich sehr viel Zeit erspart.

Dass Querdenker das Gefühl haben, dass es jetzt doch mehr Schafe gibt als früher, liegt daran, dass sie jetzt in der Lage sind über mehr Menschen in kürzerer Zeit zu urteilen. Über die meisten Menschen, die man beim Vorbeigehen gesehen hat, hat man nie nachgedacht, geschweige denn sie näher kennengelernt, um sie beurteilen zu können. Jetzt drängt es sich aber einem förmlich auf. "Dumm, dumm, nicht dumm, dumm, dumm, dumm, dumm, nicht dumm, usw."

Statt, dass die Situation nun richtig beurteilt wird – nämlich, dass der Selektionsprozess akzeleriert wurde – sehen Querdenker darin den Verfall der Gesellschaft. Davor, dass es eine Impflicht geben wird, fürchten sie sich. Die Impfung wird aber nichts im Leben desjenigen verändern, der sie nicht nehmen will (selbst bei einer Pflicht wird er Wege finden, sie nicht nehmen zu müssen), nur die Schwachen werden davon betroffen sein (mangelnde Intelligenz ist ebenfalls eine Schwäche). Die Gesellschaft verfällt also nicht durch die Maßnahmen, sondern sie hat den Kern des Verfalls schon vorher in sich getragen, die Maßnahmen bringen ihn nur ans Tageslicht.

## Sei kein Querdenker
Akzeptiere die Realtität wie sie ist, lebe nicht in Idealen. Die richtige Position hast du schon; wenn du jetzt noch die zwei obigen Fehler vermeidest, kannst du aufhören ein Querdenker und anfangen ein Straightdenker zu sein.