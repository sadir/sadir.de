---
layout: post.njk
title: "Der Grund von allem. Oder: Die Buchstabenanalogie"
date: 2020-09-04
---
In meinem [letzten Post](/posts/das-evolutionaere-prinzip/) hatte ich zu Beginn schon kurz das hermetische Prinzip der Entsprechung angesprochen. Von allen sieben Prinzipien beschäftigte mich in den letzten Jahren dieses, das zweite, am meisten. Zumindest ist es das zweite Prinzip laut dem Kybalion. Wie sehr die Interpretation des Kybalions tatsächlich der wahren Bedeutung der Corpus Hermetica entspricht, kann natürlich angezweifelt werden. Betrachtet man die Tabula Smaragdina, wird sogar direkt mit dem zweiten Prinzip eingestiegen. Da ich leider keine der Sprachen beherrsche, in denen uns die ältesten uns noch bekannten Versionen der Tabula vorliegen, habe ich mich nach deutschen und englischen Übersetzungen umgesehen. Von allen gelesenen hat mich die [Übersetzung von Isaac Newton](http://webapp1.dlib.indiana.edu/newton/html/metsnav3.html#mets=http%3A%2F%2Fpurl.dlib.indiana.edu%2Fiudl%2Fnewton%2Fmets%2FALCH00017&page=3) am meisten angesprochen:

> *Tis true without lying, certain & most true.*  
>  
> *That which is below is like that which is above*  
> *& that which is above is like that which is below*  
> *to do the miracle of one only thing*

Für mich gehört das zu den mächtigsten Versen, die ich jemals gelesen habe. Vorausgesetzt natürlich, dass sie tatsächlich so gemeint sind, wie ich sie verstehe. Dadurch, dass sie so allgemein und metaphorisch gehalten sind, interpretieren Menschen alles mögliche da hinein. Wenn man im Internet danach sucht, stößt man auf lauter esoterisch angehauchte Seiten und Videos. Ohne jetzt abzutun, wie diese Leute es verstehen, aber ich glaube nicht, dass damit nur die Relation zwischen Geist und Universum und die Übereinstimmung zwischen dem, was im Geist ist mit dem, was im Universum ist (also das Gesetz der Anziehung), gemeint war. Dieses Prinzip verstehe ich eher als die Antwort auf die Fragen, woher alles kommt und was der Welt und ihren Phänomenen zugrunde liegt. Und als eine Möglichkeit weise Voraussicht zu haben und auch angesichts der Dinge, die auf der Welt passieren, eine stoische Grundhaltung zu entwickeln. Um dies zu veranschaulichen, hatte ich vor einigen Jahren ein Modell dazu entwickelt:

## Die Buchstabenanalogie

Betrachten wir das Alphabet, stellen wir fest, dass es dem Standardmodell der Elementarteilchen entspricht. Denn Buchstaben sind die kleinsten Elemente der geschriebenen Sprache. Du kannst die geschriebene Sprache nicht weiter aufteilen als in Buchstaben.

Jede der Buchstaben hat gewisse und einzigartige Eigenschaften, die sie von den anderen Buchstaben unterscheidet, z.B. die Art, wie sie geschrieben werden, die Art, wie man sie ausspricht, ihre Position im Alphabet usw.

Nehmen wir uns einzelne Buchstaben raus, bspw. **A**, **B**, **M** und **U**, können wir sie kombinieren und ein Wort daraus kreieren: **Baum**. Wir stellen fest, dass das Wort etwas neues ist, dass es wie auf magische Weise ausdrücken kann, was die einzelnen Buchstaben nicht ausdrücken konnten. Sieht man sich die Buchstaben einzeln und gesondert an, dann enthält keine von ihnen die Bedeutung, die ihre Kombination enthält. Das Wort ist an sich eine neue Ebene, die über der Buchstabenebene entstanden ist. Das heißt, das Ganze ist mehr als die Summe der Einzelzeile.

![Wortebene aus Buchstabenebene](https://files.sadir.me/posts/20200904-wortebene.png)

Das "mehr" in "mehr als die Summe der Einzelteile" ist die eigene Wesenseinheit von "Baum", die die Buchstaben, die das Wort bilden, überhaupt nicht enthalten *können*. Diese Wesenseinheit entsteht *nur* durch die Kombination.

Was meine ich mit Wesenseinheit? Ich meine damit all die Eigenschaften und Gesetze, die mit einer bestimmten Einheit in Verbindung stehen. Jeder Buchstabe ist für sich genommen eine eigene Einheit – die kleinste Einheit, die nicht weiter gespalten werden kann. Sie haben, wie oben beschrieben, gewissen Eigenschaften. "Baum" als Wort ist auch eine Einheit. Die Eigenschaften von "Baum" sind ebenfalls wie man es schreibt, wie man es ausspricht und die Position im... Wörterbuch. Wir sehen, dass sich gewisse Dinge ändern, wenn es sich um eine neue Ebene handelt. Ebenfalls enthält das Wort neue Eigenschaften, die auf der darunterliegenden Ebene nicht existieren. Zum Beispiel Doppeldeutungen, Etymologie usw.

Eine weitere interessante Beobachtung ist, dass die Eigenschaften, die von beiden Ebenen geteilt werden, von der unteren Ebene beeinflusst werden. Wie man "Baum" schreibt, hängt davon ab, wie man die einzelnen Buchstaben schreibt. Ebenso bei der Aussprache.  
Lesen wir das Wort, beachten wir nicht wirklich die Buchstaben, wir betrachten es als eigene feste Einheit, aber wenn wir uns bewusst darauf konzentrieren, können wie sie wahrnehmen – wir sehen, dass sie die Bausteine sind. Das heißt, die Buchstaben geben ihre Eigenschaften an die nächstdrüberliegende Ebene weiter... durch eine Art Bubble-Up-Effekt. Es findet also eine Vererbung statt.

Direkt die nächste Zeile der oben zitierten Übersetzung von Newton bestätigt das Ganze:

> *And as all things have been and arose from one by the mediation of one:*
> *so all things have their birth from this one thing by adaptation.*

<p class="normal">
Das bedeutet, die Eigenschaft der Wörter sind die <em>adaptations</em> der Eigenschaften der Buchstaben, aus denen sie geboren wurden.
</p>

Jetzt können wir verschiedene Wörter nehmen, z.B. **alt**, **Baum**, **der** und **ist** und sie zu einem Satz zusammenstellen: **Der Baum ist alt**. Auch hier findet wieder das gleiche Phänomen statt: Der Satz drückt etwas aus, was jedes einzelne Wort, aus dem sie besteht, nicht ausdrücken konnte. Oder auch, was jeder einzelne Buchstabe, aus dem sie im Endeffekt auch besteht, nicht ausdrücken konnte. Es ist also schon wieder eine neue Ebene entstanden. Und schon wieder wird diese Ebene von den darunterliegenden Ebenen beeinflusst. Erst erben die Wörter die Eigenschaften der Buchstaben, adaptieren sie, erschaffen neue Eigenschaften und vererben sie weiter an die Sätze, die diese wiederum adaptieren und neue erschaffen.

Wir können nun viele Sätze nehmen und sie zu einem Absatz kombinieren. Dann nehmen wir viele Absätze und kombinieren sie zu einem Kapitel. Nehmen viele Kapitel und stellen sie zusammen zu einem Buch. Nehmen viele Bücher und erschaffen damit eine Bibliothek. Und jedes Mal, wenn wir diese Kombinationen von verschiedenen Einheiten vornehmen, egal wie klein oder groß sie sind, erschaffen wir eine neue Ebene. Und immer treten die gleichen Phänomene auf: Immer gibt die dann zweithöchste Ebene ihre Eigenschaften an die neue höchste Ebene. Immer hat die neu enstandene Ebene eigene Eigenschaften und eigene Regeln, die neu und exklusiv für sie sind.

![Saztebene auf Buchstabenebene](https://files.sadir.me/posts/20200904-weitere-ebenen.png)

Und schauen wir uns ein Buch an und fragen uns, wie es kommt, dass man es so schreibt und liest, wie man es schreibt und liest, dann lässt sich die Frage erst beantworten mit der Technik und dem Geschmack der Druckerei, dann mit der Grammatik, den Regeln der Rechtschreibung und schließlich mit den festgesetzten Axiomen der Buchstaben. Alles was wir in der höheren Ebene des Buches wahrnehmen, fußt letzten Endes auf seinen Bausteinen im Alphabet.

## Wie unten, so oben

Die wirklich interessante Erkenntnis, die man daraus gewinnen kann, ist nicht etwa nur die recht offenkundige Beobachtung, dass einfache Dinge, die sich zusammenschließen, kompliziertere Dinge bilden, die ihre eigenen Eigenschaft und Regeln haben, sondern dass diese Eigenschaften **vererbt** sind. Die Eigenschaft von Molekülen, dass sie sich zusammenschließen und abstoßen mit anderen Molekülen, wurde vererbt von Atomen, die das gleiche tun mit anderen Atomen; und das wurde wiederum vererbt von subatomaren Teilchen, die genau diese Eigenschaft auf ihrer Ebene haben.

Schließen sich Moleküle mit anderen Molekülen zusammen zu größeren Gebilden, wie z.B. Zellen, dann haben auch diese wieder diese Eigenschaften. Sie können sich anziehen und abstoßen. Nicht weiter erstaunlich haben die Phänomene auf jeder weiteren Ebene, die dazukommt, ihre eigenen Erklärungsmodelle, da bei diesen Eigenschaften eigene Faktoren im Spiel sind und die Prozesse auch komplexer sind, als auf der darunterliegenden Ebene.

Bilden sich Zellen zusammen zu lebenden Organismen, geben sie diese Fähigkeiten weiter an sie. Wir als Menschen beispielsweise können Liebe und Hass empfinden, können uns streiten und versöhnen. Das ist auch alles nichts anderes als Anziehung und Abstoßung mit anderen Elementen (Menschen) auf unserer Ebene. Wie im vorherigen Absatz beschrieben, sind die Faktoren, die zu diesen Phänomenen bei uns führen aber deutlich komplexer als die bei Zellen. Alles ist vielschichtiger und enthält mehr Variablen, sodass sich sogar ganze Wissenschaften um die Untersuchung dieser Eigenschaften gebildet haben. Das Prinzip bleibt jedoch das gleiche.

Doch Menschen sind nicht die Krone dieser Kette. Wir können uns zusammenschließen mit anderen Menschen und Gruppen bilden. Oder Gemeinschaften und Städte, Staaten und Staatengemeinschaften. Immer wenn wir uns zu Gruppen zusammentun, geben wir unsere Eigenschaften an sie weiter. So können Gruppen sich wohlgesinnt sein und zusammenarbeiten oder sie können sich verstreiten und versuchen sich gegenseitig auszulöschen.

Bekriegen sich zwei Staaten, ist es auf ihrer Ebene das gleiche, wie auf unserer Ebene, wenn sich zwei Menschen streiten. Oder auf weiter unteren Ebenen Zellen, Moleküle und Atome, die sich abstoßen. Hass und Krieg existiert, weil subatomare Teilchen mit entgegengesetzen Ladungen existieren. Sie aus der Welt zu verbannen ist unmöglich. Wenn einem eine Fee erscheint und nach einem Wunsch fragt und man sich Weltfrieden wünscht und dass sich alle Menschen nur noch lieben sollen bis ans Ende der Zeit, dann würde sich die Welt auflösen. Denn die Prinzipien, die die Welt zusammenhält wären mit ihr verschwunden. Der Fee würde nur eine von beiden Optionen offenstehen: entweder schafft sie die Abstoßung zweier Elemente ab *oder* sie schafft die Tatsache ab, dass Eigenschaften an obere Ebenen vererbt werden (was Teil des evolutionären Prinzips ist). Ohne eines von beiden könnten wir nicht hier sein.

Hass und Streit und Konflikte werden also da sein, solange es Leben gibt. Krieg könnte aber rein theoretisch abgeschafft werden, wenn man Staaten abschafft. Wenn eine Ebene verschwindet, verschwinden auch all ihre Eigenschaften mit ihr.

## Wie oben, so unten

Anziehung und Abstoßung sind jedoch nur Beispiele, die ich herausgesucht habe, um dieses Prinzip zu verdeutlichen. Ich hätte auch Eigenschaften wie Bewegung, Rhythmus, Schwingung usw. nehmen können. Ebenso hätte man statt Lebewesen bspw. Himmelsobjekte und das Universum heranziehen können. Gravitation ist auf der kosmischen Ebene die Anziehung und Dunkle Energie die Abstoßung.

Aber nicht nur solche grundlegenden Dinge werden vererbt, sondern auch komplexeres wie die Psyche. Schließen sich Menschen zu einer Gruppe zusammen, dann hat die Gruppe einen eigenen Kopf. Das äußert sich in der Massenpsychologie. Aber auch Mobmentalität ist nichts anderes. Der Mob handelt auf eine gewisse Weise, wie jeder einzelne darin auf sich alleingestellt nicht gehandelt hätte. Das heißt, sie ist ein eigenes Element, ein eigenes Organismus. Genauso haben auch Staaten eine eigene Psyche. Lesen wir in den Nachrichten oder in Geschichtsbüchern, dann wird über Länder so geschrieben, als wären sie Menschen - Land A verärgert Land B, Land C misstraut Land D etc. Wir wissen zwar, dass nur einzelne Menschen für diese Entscheidungen verantwortlich sind und nicht die Gesamtheit des Landes, aber das gleiche gilt auch für uns; wenn wir verärgert sind, ist nur ein kleiner Teil unseres Körpers für die Emotion verantwortlich, der meiste Teil hat nichts damit zu tun.

Diese Art die Welt zu betrachten, dass also ein Zusammenschluss von Dingen ein eigenes neues Element ist, sogesehen ein eigener Körper, ist ausschlaggebend. Das ist die Wahrheit, die allem zugrunde liegt. Wenn man das einmal verinnerlicht hat, sieht man es überall. Bin ich z.B. in einer Menschenmasse und wir gehen durch einen engen Durchgang, muss ich immer an Wassermassen denken, die durch eine Enge getrieben werden.

![Wasser und Menschenmenge](https://files.sadir.me/posts/20200904-wasser-menschenmenge.png)

Nun mag es erst einmal trivial erscheinen, aber das ist es keinesfalls. Dieses Verhalten, die Massen von Wassermolekülen haben, haben auch Massen von Menschen. Wir als Menschen sind im Prinzip auch Atome, nur deutlich komplexer.

Wir müssen uns vor Augen führen, dass je größer die Zusammenschlüsse sind, desto größer werden auch die Auswirkungen dieser Phänomene sein. Dagegen, dass diese Phänomene bei jeder Größenordnung auftreten werden, können wir nichts tun, es ist ein universales Prinzip. Wir können aber etwas dagegen tun, immer größer und globaler zu denken. Es wird Konflikte geben, so oder so. Ein Konflikt zwischen zwei Menschen oder zwischen zwei kleinen Gruppen wird aber nicht so eine Konsequenz mit sich ziehen, wie Kriege zwischen Staaten und sogar Staatenzusammenschlüssen.

## Europa

Der Trend geht aber leider immer weiter in Richtung globaler Gemeinschaften. In der naiven Annahme, das würde ein Zeitalter des Friedens einläuten. Zusätzlich zu der genannten Ignoranz gegenüber universellen Prinzipien, kommt hier aber noch die Tatsache hinzu, dass je größer eine Struktur ist, desto fragiler ist sie. Um so ein globales System aufrechtzuerhalten, bedarf es sehr viel Bürokratie, eines Überwachungs- und Zensurapparats und gleichgeschalteter Medien. Also einer Dystopie, wie sie im Buche steht. In der Realität gibt es aber keinen Unterschied zwischen Dystopie und Utopie. Die Utopie ist das Ideal und um sie umzusetzen, bedarf es eines dystopischen Systems.  
Die Menschen in so einer Gemeinschaft würden aber nicht merken, dass sie in einer Dystopie leben. Ihr einziger Zugang zu Informationen würde vom System selbst angeboten werden. Sie würden denken, die positiven Dinge auf der Welt würden von dem System ermöglicht werden und für die negativen Dinge wären "die anderen" verantworlich. Um die Probleme zu lösen, müsste man die anderen aber nur an den Tisch holen und noch größere Gemeinschaften schließen. Dann würde es irgendwann keine Kriege und Konflikte mehr geben.