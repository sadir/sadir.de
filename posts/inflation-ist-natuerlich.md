---
layout: post.njk
title: Inflation ist natürlich
date: 2022-07-31T19:15
---
Alles, was nicht in Gebrauch ist, schwindet mit der Zeit. Seien es nicht mehr beanspruchte Muskeln, die sich zurückbilden oder Gehirnkapazität und nicht mehr angewendetes Wissen, das mit der Zeit vergessen wird. Wieso sollte also Geld, das einfach nur auf dem Konto herumliegt, nicht an Wert verlieren? Es ist nur konsequent und im Einklang mit allen anderen Phönomenen im Universum.