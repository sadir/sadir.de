---
layout: post.njk
title: Aus der Zeit gefallene Wörter
date: 2023-01-08T14:00
---
Ein Wort wie "Diener" wird im Alltagssprachgebrauch fast nur noch abwertend verwendet. In älterer Litertur sieht man aber öfter, dass sich Leute selbst so bezeichnen. Etwa beendet Cervantes die Widmung des zweiten Buches von Don Quixote an den Grafen Lemos mit

> E. E. Diener  
> Miguel de Cervantes Saavedra

Dass sich die Bedeutung von Wörtern im Laufe der Zeit verändern ist nun nichts Ungewöhnliches, aber es ist seltsam, dass andere Wörter wie "Dienst" eine positive Bedeutung beibehalten haben. Jemand, der einem einen Dienst erweist, ist aber ein Diener. Wie kann also das eine positiv, das andere negativ sein? Und von Bediensteten wollen wir gar nicht erst anfangen.

Ähnlich verhält es sich mit dem Wort "Weib". Alle Literatur vom 19. Jahrhundert abwärts verwendet es als ganz normale Bezeichnung für "Frau", aber heute wird es von jedem als abwertend wahrgenommen. Aber was ist dann mit "weiblich"? Ganz normales Wort.