---
layout: post.njk
title: Das Intelligenz-Dilemma
date: 2021-02-23T07:00
---
Dummheit vs. Intelligenz ist ein sehr interessantes Thema. Viel wurde schon darüber gesagt und jedes Mal, wenn Dummheit adressiert oder sich darüber lustig gemacht wird mit Sätzen wie

> ![Kurt Tucholsky](https://files.sadir.me/posts/20210223-kurt-tucholsky.jpg)
> **Der Vorteil der Klugheit besteht darin, dass man sich dumm stellen kann. Das Gegenteil ist schon schwieriger.**  
> – Kurt Tucholsky

<p class="normal">pflichten Leute dem bei und müssen vielleicht auch an gewisse Menschen denken, auf die das zutrifft, aber niemand identifiziert sich selbst mit den Dummen in diesen Zitaten. Es ist aber reine Mathematik, dass viele Menschen, die hierbei an andere denken müssen, selber die Dummen sind. Die Frage ist nur, wie findet man heraus zu welcher Gruppe man gehört? Denn ganz offensichtlich fehlt es dem Gehirn dummer Menschen an genug Reflexionsvermögen, um diese Tatsache zu erkennen. Warum es so ist, beschreibt uns der Dunning-Kruger-Effekt:</p>

> ![David Dunning](https://files.sadir.me/posts/20210223-david-dunning.jpg)
> **Wenn man inkompetent ist, kann man nicht wissen, dass man inkompetent ist […]. Die Fähigkeiten, die Sie benötigen, um eine richtige Antwort zu geben, sind genau die Fähigkeiten, die Sie benötigen, um zu erkennen, was eine richtige Antwort ist.**  
> – David Dunning

Erkennt jemand wie wenig er weiß, besitzt er schon mal genug mentale Kapazitäten, um vielleicht nicht direkt intelligent, aber um zumindest nicht dumm zu sein. Hält sich aber jemand für intelligent, bedeutet es dann im Umkehrschluss, dass er höchstwahrscheinlich dumm ist? Nicht zwangsläufig, aber es ist eine Option, die er offenhalten sollte. Viele Menschen halten es bei der Beurteilung, in welche Kategorie sie sich einordnen, für wichtig, sich mit anderen zu vergleichen. Das kann aber oft irreführend sein.

Ich habe manchmal Videos gesehen, in denen jemand über tiefgründigere oder komplizierte Themen geredet hat, aber seine Standpunkte in meinen Augen mich an mich selber erinnert haben, als ich noch naiver und unreifer darüber gedacht habe. Schaue ich aber in die Kommentare, sehe ich viele Leute, die ihn für seine Takes loben und sogar sagen, dass sie etwas aus diesem Video gelernt haben. Wenn er sich diese Kommentare durchliest, wird er sich bestätigt fühlen und denken, dass er intelligent ist, immerhin schreiben das so viele Leute. Dass er hier der Einäugige unter den Blinden ist, wird ihm natürlich nicht auffallen.

Umgekehrt gibt es auch Fälle, in denen man selber von sich überzeugt ist, aber alle um einen herum sagen, dass man dumme Sichtweisen hat. Sollte man das auf jeden Fall als Bestätigung sehen, dass man Opfer des Dunning-Kruger-Effekts ist? Wieder gibt es keine eindeutige Antwort, denn es kommt darauf an *wer* diese Personen sind und wie korrumpiert und künstlich der Zustand der Gesellschaft ist. Denn was auch immer der Konsens in der Gesellschaft ist, die meisten Menschen werden ihm anhängen und

> ![Jiddu Krishnamurti](https://files.sadir.me/posts/20210223-jiddu-krishnamurti.jpg)
> **Es ist kein Zeichen von Gesundheit, an eine von Grund auf kranke Gesellschaft gut angepasst zu sein**  
> – Jiddu Krishnamurti

Wie man es auch dreht und wendet, die Meinung anderer wird uns keine eindeutige Antwort darauf geben können. Nicht mal, wenn die Gesellschaft krank ist und die meisten gegen uns sind, könnten wir mit Sicherheit zu dem Schluss kommen, dass wir demnach die vernünftige Sichtweise haben, denn es könnte auch sein, dass die Gesellschaft in Wirklichkeit gar nicht krank ist und uns nur unsere Dummheit dazu führt das zu denken. Wir könnten am Ende nur Gefangene unserer eigenen Inkompetenz und Opfer unserer falschen Urteile sein.

## Intelligenztests

Selbst wenn wir etablierte Methoden, unsere Intelligenz zu messen, wie den IQ-Test, zu Rate ziehen, können wir uns nicht darauf verlassen. Denn alles was dieser Test macht ist es, Intelligenz in verschiedene Kategorien, wie Mustererkennung, Vervollständigung von Zahlenreihen, Sprachverständnis etc. einzuteilen und Aufgaben für diese zu entwerfen, um zu schauen, wie gut wir abschneiden. Die Aufgaben selbst sind aber völlig künstlich und gleichen nichts, was wir im echten Leben oder in der Natur begegnen würden. Schneiden wir gut in diesen Tests ab, bedeutet es nicht, dass wir intelligent sind, sondern dass wir gut darin sind, menschengemachte Aufgaben zu lösen.

Verfechter von IQ-Tests werden aber einwenden, dass Statistiken eindeutig zeigen, dass es eine Relation zwischen einem hohen IQ und späterem Erfolg im Leben gibt, z.B. in Form eines gutbezahlten und angesehenen Berufes. Was sie aber nicht bedenken ist, dass es dafür keiner Statistiken bedarf und es ein völlig naheliegender Zusammenhang ist, denn die meisten Menschen, die in unserem System einen gutbezahlten Beruf haben, haben auch einen guten Abschluss. Einen guten Abschluss haben bedeutet im Grunde genommen nur, dass sie während ihrer Ausbildungszeit gut darin waren, die Tests des Lehrers zu lösen. Jemand, der einen menschengemachten IQ-Test meistert, der meistert höchstwahrscheinlich auch eine menschengemachte Prüfung in der Schule. Nichts von beiden misst die Intelligenz, es misst nur die Fähigkeit menschengemachte Aufgaben zu lösen.

Man darf nicht vergessen, dass sobald man Intelligenz in seine Einzelteile zerlegt und jedes Teil einzeln misst, man zu einem verzerrten Ergebnis kommen wird, denn man kann dann nicht mehr einfach die Resultate der Einzelteile addieren, um zu dem Gesamtergebnis zu kommen. Das ist nicht, wie unser Gehirn funktioniert. Intelligenz funktioniert nur als Ganzes.

Alles, was wir extrahieren, trennen wir von ihrer Natur. Ob es Pflanzenöle sind oder Uran, sobald  wir Verfahren entwickeln, um etwas aus ihrem Element zu isolieren, ist der isolierte Teil für uns ungesund bis lebensgefährlich, denn er ist unnatürlich. Genauso unnatürlich ist es, einen Teil wie Mustererkennung zu isolieren und einzeln zu messen. Der Zweck dieser Tests ist es also nicht unsere natürlich Intelligenz zu ermitteln, sondern nur die Fähigkeit uns in unserer [selbstgemachten Natur](/posts/unser-krieg-gegen-die-natur/), unserer künstlichen Zivilisation, zu bewegen. Das ist auch der Grund, wieso die reichen Industrieländer durchschnittlich einen höheren IQ haben, als Entwicklungländer. Denn der IQ misst nicht die Intelligenz, sondern die Künstlichkeit der Gesellschaft bzw. den Grad, inwieweit sie sich von der Natur getrennt haben.

## Was ist Intelligenz?

Man kann nicht anders, als zu dem Schluss zu kommen, dass Intelligenz nichts greifbares ist. Ja, dass es vielleicht noch nicht einmal existiert, denn hat es überhaupt eine Rolle gespielt, bevor es die ersten Zivilisationen gab? Bedeutete Intelligenz nicht einfach nur, dass man am Leben war und demnach nichts Dummes getan hat, wodurch man umkam und je älter man war, desto intelligenter war man auch, denn das bedeutete, dass man umso länger kluge Entscheidungen getroffen hat, die einen am Leben gehalten haben? Und war das nicht der Ursprung dafür, dass in Erzählungen alt sein ein Synonym für weise sein ist?