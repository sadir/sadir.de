---
layout: post.njk
title: Revolutionen sind zwecklos
date: 2020-11-08T02:15
---
Wieviele Revolutionen gab es nicht schon in der Menschheitsgeschichte und doch sitzen politische Aktivisten immer noch beisammen und sinnen über die nächste. Hätte nur eine von den vergangenen ihren Zweck erfüllt, wäre das jetzt kein Thema mehr und wir würden in dieser perfekten utopischen Welt leben. Das gewaltsame Umstürzen eines Systems hat nur zur Folge, dass das gleiche System wieder aufgebaut wird, nur in anderen Farben. Denn was kann nur entstehen aus der Asche eines verbrannten Phönix? Richtig, ein neuer Phönix. Für wahren Wandel sollte man sich viel mehr darauf fokussieren die Gesellschaft zu transformieren, von innen heraus. Alles andere ist nur Schein.