---
layout: post.njk
title: Was kontrollierte Opposition wirklich ist
date: 2021-10-11T07:10
---
Unter kontrollierter Opposition verstehen Leute oft, dass es geheime Agenten sind, die von der Regierung oder einer anderen Organisation eingesetzt werden, um sich als Systemgegner auszugeben und die Bevölkerung mit verdrehten Tatsachen bewusst hinters Licht zu führen. Auch wenn es solche Leute bestimmt gibt, denke ich, dass die meisten eher als **geduldete Opposition** bezeichnet werden sollten.

Ganz gerne wird der Vorwurf gegen Verschwörungstheoretiker gemacht. Doch was sind die Merkmale einer kontrollierten bzw. geduldeten Opposition?

Unter Systemgegnern gibt es, wie auch unter allen anderen Medienmachern, einige wenige große Namen mit Hunderttausenden bis zu mehreren Millionen Zuschauern oder Lesern und einige Leute, die kaum stattfinden; die sehr schwer aufzufinden sind, wenn man nicht ganz genau weiß, wonach man suchen soll und selbst wenn man gezielt nach ihnen sucht, tauchen ihre eigenen Inhalte oft weiter unten in den Ergebnissen auf. Wieso bekommen einige so eine große Bühne, während andere begraben werden, obwohl sie beide im Prinzip gegen das gleiche System sind? Was ist der Hauptunterschied zwischen ihnen?

Um es mit einer Anlehnung an ein bekanntes Sprichwort zu sagen: Die Gegner, die geduldet werden, geben einem Fische, während die Gegner, die nicht geduldet werden, einem beibringen zu fischen.

Die geduldeten Gegner betreiben meistens ihre eigenen Nachrichtenkanäle und verarbeiten die täglich hereinkommenden Informationen, während die nicht geduldeten einem generelles Wissen an die Hand geben, womit man bis ans Lebensende alle Informationen selber unabhängig verarbeiten kann.

Ist man nämlich abhängig vom Nachrichtenkanal der Opposition, ist es das gleiche wie abhängig zu sein von den Nachrichtenkanälen der Mainstreammedien. Heute kann ich das eine glauben und wenn in einer Woche scheinbar neue Informationen hereinkommen, muss ich meine Meinung wieder ändern. So kann man innerhalb einer kurzen Zeit viele gegensätzliche Aussagen machen – mit der Begründung, dass ständig neue Erkenntnisse gemacht wurden – und so für mehr Verwirrung als Erleuchtung sorgen. Die geduldete Opposition schlägt in exakt die gleiche Kerbe, denn sie hilft dabei (unfreiwillig?) die Leute zu verwirren und unsicher zu machen. Das ist der erste Grund, wieso sie geduldet wird.

Der zweite Grund ist, weil sie immer gewisse Aspekte des Systems angreifen und nicht das System als Ganzes. Das liegt in der Natur ihrer Vorgehensweise, denn wenn sie die aktuellen Geschehnisse angreifen und nicht etwa das darunterliegende System, stellen sie keine ernstzunehmende Gefahr für das System dar.

Um es an einem aktuellen Beispiel zu verdeutlichen: Seit der COVID-19-Pandemie und insbesondere seit der Impfung gibt es viele Leute, die sich öffentlich dagegen aussprechen und all die Leute, die das propagieren, gezielt angreifen, teils mit mehreren Millionen Aufrufen – dennoch dürfen sie stattfinden. Denn der Grund liegt an ihrer Argumentationsweise. Greift jemand die COVID-19-Impfung an, vergleicht er das bspw. mit der Masern-Impfung und stellt klar, dass letzteres wenigstens eine richtige Impfung ist und ersteres Gift oder ein Mikrochip oder was auch immer.

Jemand, der so denkt und sich äußert, ist für die Pharmaindustrie nur ein Witz und stellt keine Gefahr dar. Denn wenn sie dich dazu bringen konnten zu glauben, dass die Masern-Impfung ungefährlich ist, gibt es keinen einzigen Grund, wieso sie dich nicht überzeugen können sollten, dass die COVID-19-Impfung ungefährlich ist. Sie müssen nur einige Karten richtig spielen und all die Leute, die sich heute als Opposition präsentieren, werden fallen wie kleine Spielfiguren.

Ganz gut kann man das am Beispiel des Ursprungs des Virus sehen. Als es hieß, dass es doch nicht aus einem Markt in Wuhan kam, sondern in einem Labor hergestellt wurde, sind plötzlich alle auf diesen Zug aufgesprungen. Die *gleichen* Leute, die bisher darüber geredet haben, dass die Medien zu diesem Thema ohne Ende lügen, haben nun den *gleichen* Medien geglaubt und sie zitiert. Diejenigen, die das verbreitet haben, wussten genau welche Knöpfe sie drücken mussten und haben es an einem realen Beispiel in Echtzeit demonstriert und für sich selbst abermals bestätigt, dass die Leute unwissend sind.

Was wäre aber, wenn die nicht geduldete Opposition mehr Reichweite hätte? Wie würden die für weniger Verwirrung und mehr Klarheit sorgen? Die würden z.B. ganz klar und logisch darlegen können, dass jede Impfung, die jemals hergestellt wurde, hochgiftig ist, denn jeder einzelne Inhaltsstoff ist für sich genommen giftig, wie können sie zusammengemischt plötzlich nicht giftig sein? Die könnten einem erklären, woher sogenannte Infektionskrankheiten und Pandemien kommen und was Impfungen (generell, nicht nur die COVID-19-Impfung) für ein gesundheitliches Risiko darstellen. So würde jeder verstehen können, dass Impfungen per definitionem toxisch sind, ja, dass sie sogar ein Synonym für Gift sind und wären dann immun gegen jede weitere zukünftige Falschinformation, die durch Medien und sogenannten Experten verbreitet werden.

Das wäre ein riesiger Profiteinbruch für die Pharmaindustrie. Aber das ist nur ein Produkt. Zusätzlich könnte eine richtige Opposition einem erklären, dass jede Medizin, also *alles* was man in Apotheken bekommen kann, d.h. im Labor hergestellt wurde, giftig ist und nicht dafür gedacht ist uns zu heilen, sondern nur Symptome zu unterdrücken. Dass sogar jede Behandlung, die in Krankenhäusern gemacht wird, sei es eine Magenverkleinerung, eine Chemotherapie oder das Anschließen an ein Atemgerät extrem gesundheitsschädlich ist.

Diese richtige Opposition würde einem erklären können, woher Krankheiten wirklich kommen und wie man sie behandelt, ohne Chemie und ohne Hokuspokus.

Wie will man jemanden mit diesem Wissen jemals mit tödlichen Pandemien in Panik versetzen oder ihnen mit magischen Heilmitteln, die irgendwelche Nerds in Laboren hergestellt haben, Hoffnung geben? Es wäre für diese Industrie nicht mehr möglich sie zu kontrollieren oder an ihnen Geld zu verdienen.

Genau deswegen werden einige Leute unterdrückt und andere sind sehr präsent. Man kann sogar davon ausgehen, dass jeder Systemgegner, der viele Klicks hat und dennoch stattfinden darf, nicht das große Bild präsentiert und zumindest zu einem gewissen Maße zur Verwirrung beiträgt.