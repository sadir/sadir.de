---
layout: post.njk
title: Der versteckte Zweck von Filmen
date: 2020-10-04T01:25
---
Ich habe schon seit längeren irgendwie den Spaß an Filmen verloren. Es ist nicht so, dass sie mich nicht unterhalten, aber ich müsste mich schon überwinden einen neuen zu schauen und während ich ihn gucke, fallen mir immer wieder Szenen auf, die ihn mir ruinieren. Es sind meistens Versuche den Zuschauer zu manipulieren, z.B. in Form von politischer Propaganda.

Ob es nun Karate Tiger ist, der während des Kalten Krieges lief und in dem der böse und gefülslose Antagonist aus Russland kommt oder eine Serie wie Narcos: Mexico, indem sich die Drogenbarone immer in die Hosen machen, wenn die Amerikaner hinter ihnen her sind und man jeden töten kann, aber bloß nicht einen DEA-Agenten, weil sonst gefriert die Hölle zu – wer soll das ernst nehmen? Generell versucht jeder Film die Message rübrezubringen, dass sich Kriminalität auf keinen Fall lohnt und dient somit als Tool des Staates.

Das ergibt aber auch Sinn, wenn man sich die Anfänge vom modernen Film und Fernsehen anschaut. Ob es die Nazis waren, die es ganz offensichtlich als Propaganda benutzt haben oder auch die Amerikaner, die u.a. mit Disneys die Gegenpropaganda gegen die Nazis gemacht haben, war es von Anfang an ein optimales Medium um in die Köpfe der Menschen zu gelangen. So dass bis heute Geheimdienste in Hollywood mitmischen und die Interessen des Staates durchbringen. Je mehr Zeit vergeht, desto mehr zweifle ich an der ganzen Sache. Es geht sogar so weit, dass ich mich frage, ob es überhaupt so eine riesige Industrie wäre oder geworden wäre, wenn der Staat keine Verwendung für sie hätte... Ob Propaganda nicht der Hauptzweck ist und Unterhaltung ein positiver Nebeneffekt.