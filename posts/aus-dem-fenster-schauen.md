---
layout: post.njk
title: Aus dem Fenster schauen
date: 2021-06-21T16:45
---
Ich habe realisiert, dass wann immer ich am Schreibtisch sitze und etwas lese, das mich zum Nachdenken bringt, es das Denken verfeinert und vertieft, wenn ich aus dem Fenster schaue. Das hat einen stärkeren Effekt, wenn man in einem höheren Stockwerk sitzt und so die Sicht nicht durch andere Häuser blockiert wird. Das in die Ferne schauen scheint irgendetwas im Gehirn anzuregen. Möglicherweise liegt es daran, dass unsere Augen genau dafür konzipiert wurden und wir auf die Weise im Einklang mit unserem eigenen Wesen sind. Wenn sich ein Bildschirm oder ein Buch unmittelbar vor unserem Gesicht befindet, scheint das Denken in einen anderen Modus zu schalten. Es ist, als ob in die Ferne schauen einen wieder erdet und zu einem selbst zurückbringt.

Dem Spazieren gehen wird nachgesagt, dass es das Denken anregt. Vielleicht ist nicht das Laufen dafür verantwortlich, sondern die Tatsache, dass man dabei in die Ferne schaut. Es wäre interessant zu untersuchen, ob es den gleichen Effekt hat, wenn man beim Spazierengehen ein Objekt vor das Gesicht hält, so dass man gezwungen ist einen Punkt aus nächster Nähe zu fixieren.