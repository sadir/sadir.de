---
layout: post.njk
title: Menschen und Politiker
date: 2020-09-07T20:10
---
So lustig und spottanfällig die Reptiloidentheorie auch sein mag, ich glaube Schuld an ihrem Entstehen sind Politiker selbst. Genauso wie das klassische Bild eines Arbeiters, der nach Feierabend mit seinen Kollegen in der Kneipe sitzt und bei einem Bier über "die da oben" lästert und dass "sie ja sowieso machen würden, was sie wollen", verrät die Theorie darüber, dass die Mächtigen in Wahrheit Echsenmenschen aus dem Weltall sind, viel über die Entfremdung der Repräsentaten von den Repräsentierten.

Abgesehen davon natürlich, dass die Politiker *tatsächlich* nicht das Volk repräsentieren und es auch gar nicht ihre richtige Aufgabe ist,<sup>[[1]](#fn-1)</sup> liegt die Ursache für viele, die denken, dass sie es tun oder wenigstens tun sollten, eher an dem komplett entmenschlichten Verhalten. Politiker sind wie programmierte Roboter, darauf ausgelegt gewisse Aufgaben zu vollführen und ja nicht natürliche und vertraute menschliche Regungen zu zeigen. Wenn mal das Menschliche durchscheinen soll, dann bitte nur kalkuliert – gehe rum und zeig dich volksnah, nimm ein Kind in den Arm und spiel etwas mit ihm rum... aber pass ja auf, dass die Kameras auf dich gerichtet sind!

Wenn du Interviews gibst und dir eine Frage gestellt wird, dann darfst du auf keinen Fall direkt darauf antworten, egal was passiert, denn sowas machen nur Menschen. Du hältst Reden? Dann sollten die Art wie du redest, deine Betonungen, deine Sprachmelodie und auch deine Gestik einstudiert sein. Du hast keine Ahnung wie das geht, hol dir Berater ins Team. Die werden dir zeigen, wie man sich als ein richtiger Politiker verhält, niemand möchte einem Menschen zuhören.

Jemand greift dich öffentlich an, beleidigt dich sogar? Wird persönlich? Zeig dich auf jeden Fall völlig unberührt davon. Bestenfalls ignorierst du es komplett und wenn du darauf angesprochen wirst, sagst du einfach emotional unberührt irgendwas, ganz egal was, muss nicht mal wirklich um das Thema gehen. Am besten ist es, wenn man eine Minute später sich nicht mal mehr erinnert kann, was du eigentlich gesagt hast. Verärgerung, Wut, Rachegefühl usw. sind niedere menschliche Emotionen, sowas hast du nicht nötig. Das kannst du der ungewaschenen Masse überlassen, die du rein theoretisch repräsentierst. Freude, Stolz, Glück usw. kannst du auch vergessen. Jede Emotion muss wohlüberlegt nach Außen gebracht werden, dein Grundzustand ist jedoch neutral.

<hr class="short">

Ich überlege mir manchmal, wie gut wäre es eigentlich, wenn Politiker, die ja sowieso ständig angegriffen werden, ständig Ziel von Comedians und Satireshows sind, einfach mal einmal kontern könnten. Bei ihrem nächsten öffentlichen Auftritt oder wenn sie darauf angesprochen werden, zurückschießen, irgendwelche Sprüche bringen. Und wenn jemand wirklich unter die Gürtellinie gegangen ist, einfach frech zurückantworten, ihn beleidigen. Wieso immer dieses emotional unberührte, damit kann sich kein Mensch identifizieren; zumindest nicht die breite Masse.

Wieso ist es so schwer ein normaler Mensch zu sein? Wieso ist es so schwer zu zeigen, was man denkt und fühlt? Und ich rede gar nicht davon, sich zu sehr emotional da reinzusteigern, sondern einfach seinen Mann oder seine Frau zu stehen. Zeig, dass du dich nicht hast von deinem Beruf verbiegen lassen und du immer noch einen Rückgrat hast. Zeig, dass du stark und unabhängig bist und nicht nur eine leere Hülle im Anzug.

Das alles ist mit die Ursache für die Entfremdung der Politiker vom Volk. Das meiste passiert wahrscheinlich unbewusst. Wir sind soziale Wesen und im Laufe der Evolution darauf entwickelt worden, sofort Menschen herauszufiltern, mit denen etwas nicht stimmt, die potentiell eine Gefahr sein könnten, weil sie sich nicht gewöhnlich verhalten. Das passiert ganz automatisch, ohne dass wir darüber bewusst nachdenken müssen. Fragt man uns aber, wieso wir sie nicht mögen, würden die meisten wahrscheinlich sagen "Das sind Lügner", "Die sind unehrlich". Und das ist richtig, es ist unaufrichtiges Verhalten, demnach lügen sie über ihre Absichten. Unser "Reptilienhirn" (no pun intended) ist ein ausgeklügeltes System und sagt zuverlässlich, wen wir vertrauen sollten und wen nicht.

Die von ihnen nicht beabsichtigte Konsequenz davon ist aber nicht einfach, dass die meisten dann keinen Bock mehr auf Politiker haben, sondern dass sie Bock auf *andere* Politiker haben. Auf Populisten, auf Demagogen. Auf Leute, in denen sie sich in irgendeiner Weise wiederspiegeln können. Dieses "Ich will jemanden, der mal ordentlich durchgreift" ist nicht selten ein "Ich will jemanden, der etwas macht, was ich machen würde, wenn ich die Fäden in der Hand hätte". Sie wollen einen von ihnen, einen, der wie sie ist, nicht nur einen, der vorgibt, einer von ihnen zu sein, aber in jeder Facette seines Seins das Gegenteil präsentiert. Wie schwer kann es sein, jemand zu sein, als der man geboren ist?

---
<ol class="fns">
    <li id="fn-1">Ich sagte bereits dazu etwas in "<a href="/posts/das-evolutionaere-prinzip/">Das evolutionäre Prinzip</a>".</li>
</ol>