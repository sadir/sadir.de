---
layout: post.njk
title: Der wahre Ursprung vieler geistiger Zustände
date: 2021-07-21T15:45
---


Einer der Irrtümer der modernen Psychologie und Soziologie ist es vermutlich, dass psychischen Zuständen psychische Ursachen zugeschrieben werden. Völlig unterschätzt wird dabei die Ernährung der Menschen. Dabei ist es naheliegend, dass das was man isst jeden Aspekt einer Person beeinflussen wird. Der Grund wieso Menschen in ländlichen Gegenden im Durchschnitt konservativer sind als Menschen in Großstädten ist, weil sie sich mehr so ernähren wie ihre Vorfahren. Was du isst bestimmt nicht nur über deine Gesundheit, sondern auch über deine politischen und gesellschaftlichen Meinungen und deine generelle Sicht auf die Welt. Logisch, denn dein Gehirn ist ein Organ und kann nur mit dem arbeiten, was du ihm gibst. Wenn du ständig menschengemachtes Essen mit künstlichen Chemikalien zu dir nimmst, wird es zu anderen Resultaten in allen Organen führen, als wenn du dich traditionell ernährst. Selbst Supplements, die versuchen Nährstoffe zu imitieren, haben auf molekularer Ebene eine andere Struktur (s. Abb.) und werden deshalb auch ein anderes Resultat im Körper erzielen. Industriell gefertigtes Essen führt daher unweigerlich zu einer liberaleren Weltsicht.
<figure>
    <a href="https://files.sadir.me/posts/20210721-vitamins-real-vs-supplement.jpg">
        <img alt="Vitamine - Real vs. Supplements" src="https://files.sadir.me/posts/20210721-vitamins-real-vs-supplement.jpg">
    </a>
</figure>