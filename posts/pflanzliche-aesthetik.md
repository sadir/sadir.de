---
layout: post.njk
title: Pflanzliche Ästhetik
date: 2022-04-20T23:15
---
In seinem Hauptwerk "Die Welt als Wille und Vorstellung" hat Arthur Schopenhauer einen interessanten Gedanken geäußert, wieso er glaubt, dass die Pflanzenwelt so farbenreich, ausdrucksstark und ästhetisch ist:

> *Vor Allem hat die schöne Natur diese Eigenschaft und gewinnt dadurch selbst dem Unempfindlichsten wenigstens ein flüchtiges ästhetisches Wohlgefallen ab: ja, es ist so auffallend, wie besonders die Pflanzenwelt zur ästhetischen Betrachtung auffordert und sich gleichsam derselben aufdringt, daß man sagen möchte, dieses Entgegenkommen stände damit in Verbindung, daß diese organischen Wesen nicht selbst, wie die thierischen Leiber, unmittelbares Objekt der Erkenntniß sind, daher sie des fremden verständigen Individuums bedürfen, um aus der Welt des blinden Wollens in die der Vorstellung einzutreten, weshalb sie gleichsam nach diesem Eintritt sich sehnten, um wenigstens mittelbar zu erlangen, was ihnen unmittelbar versagt ist. Ich lasse übrigens diesen gewagten und vielleicht an Schwärmerei gränzenden Gedanken ganz und gar dahingestellt seyn, da nur eine sehr innige und hingebende Betrachtung der Natur ihn erregen oder rechtfertigen kann.*

Weiter bemerkt er in der Fußnote, dass 40 Jahre nachdem er "schüchtern und zaudernd" diesen Gedanken aufgeschrieben hatte, er entdeckt habe, dass der heilige Augustinus schon zuvor genau auf den gleichen Gedanken gekommen war, als er sagte: "*Arbusta formas suas varias, quibus mundi hujus visibilis structura formosa est, sentiendas sensibus praebent; ut, pro eo quod nosse non possunt, quasi innotescere velle videantur.*" (De civ. Dei, XI, 27.) (dt. "Die Pflanzen bieten ihre mannigfachen Formen, durch die der sichtbare Bau dieser Welt sich formschön gestaltet, den Sinnen zur Wahrnehmung dar, so daß sie, da sie nicht erkennen können, wie es scheint, gleichsam erkannt werden wollen."<sup>[[1]](#fn-1)</sup>
)

Dies erinnerte mich sofort daran, wie Leute lauter sprechen, wenn sie entweder Ohrenstöpsel oder Kopfhörer im Ohr haben und die Umgebung entweder dumpfer oder gar nicht hören können. Dadurch, dass ihnen also ein Sinn (der auditive) funktionsunfähig gemacht wurde, kompensieren sie es damit, dass sie ein anderes umso lauter stellen. So könnte es also auch sein, dass Pflanzen, dadurch dass sie selbst nicht sehen können (ihnen also der visuelle Sinn fehlt), diese Tatsache damit kompensieren, dass sie sich in der Hinsicht auf noch ausdrucksstärkere und formschönere Art darstellen.

---

<ol class="fns">
	<li id="fn-1">Übersetzung aus der Ausgabe vom Haffmans Verlag.</li>
</ol>
