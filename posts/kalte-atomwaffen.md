---
layout: post.njk
title: Kalte Atomwaffen
date: 2021-02-01T01:45
---
Mir ist letztens aus irgendeinem Grund wieder eine Aussage von Gary Vaynerchuk eingefallen, die er 2019 in einem jährlichen Meeting mit seinen Praktikanten getätigt hat.<sup>[[1]](#fn-1)</sup> Auf eine Frage nach der Entwicklung von Social Media, besonders bezüglich der Sucht vieler junger Leute danach, sagte er, dass wir nicht süchtig sind nach Social Media, sondern nach menschlicher Interaktion. Dass es uns nicht verändert, sondern nur exposed. Dass der Grund, wieso wir seit 70 Jahren Atomwaffen haben, aber noch alle am Leben sind, der ist, dass wir uns mögen.

<video controls src="https://files.sadir.me/posts/20210127-garyvee.mp4"></video>

Auch wenn ich eigentlich kein Pessimist bin, glaube ich nicht, dass die Nächstenliebe der Grund ist, wieso es die Welt noch gibt. Ich hatte über dieses Prinzip in anderen Posts schon geschrieben, aber wenn es um etwas wie Atomwaffen geht, die einen unweigerlich globalen Einfluss haben werden, sind es nicht mehr einzelne Individuen, die darüber entscheiden, sondern die Staaten als eigene Einheiten.<sup>[[2]](#fn-2)</sup> Ein Staat, der sich dazu entschließen würde sie einzusetzen, würde Russisch Roulette spielen. Und solange er nicht lebensmüde ist, wird er immer Wege wählen, die die Menschen in Gefahr bringt, aber nicht selbst.

---

<ol class="fns">
    <li id="fn-1"><a href="https://www.youtube.com/watch?v=gv6_h2FDpGk">"This Is My Favorite Meeting Every Year | Fireside Chat With 2019 Summer Interns"</a> ab <a href="https://youtu.be/gv6_h2FDpGk?t=1200">20:00</a></li>
    <li id="fn-2">Siehe dazu u.a. meine Posts <a href="/posts/der-grund-von-allem-oder-die-buchstabenanalogie/">"Der Grund von allem. Oder: Die Buchstabenanalogie"</a> und <a href="/posts/die-perfekte-taeuschung/">"Die perfekte Täuschung"</a></li>
</ol>