---
layout: post.njk
title: Wiederkehrende Muster in Menschengruppen
date: 2021-05-25T15:00
---
Wie kommt es eigentlich, dass verschiedene Menschengruppen die gleichen Muster aufweisen? Dass wenn man eine zufällig ausgewählte Gruppe von Menschen nimmt und beispielsweise deren IQ oder deren Noten bei einer Prüfung anschaut, sich diese in einer Normalverteilung, in einer Glockenkurve, werden plotten lassen? Wie kommt es, dass gewisse Eigenschaften in einer Pareto-Verteilung erscheinen, wie z.B. die Vermögensverteilung in einer Bevölkerung, bei der in der Regel 20% der Menschen 80% des Einkommens besitzen oder bei einem Projekt, bei dem 20% der Arbeit 80% des Resultats bringt? Wieso treten bei verschiedenen Zusammenstellungen und in verschiedenen Kontexten immer wieder die gleichen Muster auf?

Die Frage lässt sich auf die gleiche Weise beantworten wie die Frage, wieso verschiedene Menschen, mit ihren eigenen Zellen, ihren eigenen Enzymen und Bakterien dennoch gleiche Eigenschaften und biologische Prozesse teilen; wieso verschiedene Spezies dennoch die gleichen Organe haben, wieso also Mensch und Papagei beide ein Herz, ein Gehirn usw. haben.

Jede Menschengruppe, die groß genug ist, ist ein eigenes Wesen, ein eigenes Organismus (die Hintergründe dazu habe ich bereits [in einem anderen Post beschrieben](/posts/der-grund-von-allem-oder-die-buchstabenanalogie/)). So haben die verschiedenen Gruppen die gleichen Eigenschaften, wie verschiedene Menschen die gleichen Eigenschaften haben, obwohl sie ein Set von unterschiedlichen Mikroorganismen sind. Wir verhalten uns zu diesen Gruppen auf die gleiche Weise wie die Mikroorganismen sich zu uns verhalten.