---
layout: post.njk
title: Die Massen in die Irre geführt
date: 2022-01-18T14:00
---
Lange Zeit gehörte es unter Skeptikern zum guten Ton Religionen zu kritisieren. Vor etwa zehn Jahren hatte der Neue Atheismus sogar eine Hochphase erlebt. Religion galt als Werkzeug, um die Massen zu kontrollieren. Jedoch sind viele von dieser Position wieder abgewichen. Einige sind komplett zur Religion zurückgekehrt und andere sind ihr nicht mehr so feindlich gegenübergestellt.

Doch was ist passiert? Das mag sicherlich mehrere Faktoren haben, jedoch glaube ich, dass es entscheidend für die Psyche vieler Menschen war, dass sie Dinge erlebt haben, die sie als schlecht oder böse erachten und sie deswegen daraus geschlossen haben, dass es auch ein Gut gibt. Oder um es anders zu sagen: Wer einmal den Teufel gesehen hat, glaubt im Umkehrschluss, dass es auch einen Gott geben muss.

Unter Verschwörungsvideos, die bspw. okkulte Symboliken in Hollywood-Filmen und Musikvideos behandeln, findet man oft Kommentare, die christlich angehaucht sind und Verweise auf Bibelverse beinhalten. Das bedeutet, diese Leute glauben, dass es eine satanische Elite gibt, die in ihren Augen böses tut, und schließen daraus, dass die Gegenseite (göttlich/christlich) gut sein muss; und weil es sie gibt, muss es auch die Gegenseite geben.

Was ist aber, wenn diese Elite genau das will? Dass sie diese Symbole so offensichtlich und öffentlich verbreitet, um genau diesen Effekt in Menschen auszulösen? Statt also zu sagen, dass die Menschen wieder zur Religion gehen sollen, präsentieren sie ihnen das Okkulte, das sie abstößt und wieder zur Gegenseite gehen lässt, aber gleichzeitig in ihnen auch den Anschein erweckt, als ob sie diese Entscheidung selber getroffen hätten.

Doch wieso sollte die Elite eine christliche Masse haben wollen? Ich denke, dass einige christliche Werte wie z.B. der Fokus auf Familie, für sie nicht unbedingt das Ideal sind, doch ist alles in Allem das Christentum dennoch das kleinere Übel. Solange jemand christlich ist, wird er immer Recht und Gesetz dem Gesetz der Natur bevorzugen. Ein viel größeres Problem für das System wäre nämlich das [Zurückkehren zur natürlichen Ordnung](https://jcpa.org/article/neo-paganism-in-the-public-square-and-its-relevance-to-judaism/) <sup>[[1]](#fn-1)</sup>
. Solange also der Schein vom archaischen Dualismus Gut gegen Böse oder Gott gegen Satan gewahrt wird, sind Menschen angelenkt genug, um zu bemerken, dass es auch noch andere Alternativen gibt.

---

<ol class="fns">
	<li id="fn-1">Link ebenfalls im <a href="https://www.sadir.me/archive/jcpa.org/1650052022447/article/neo-paganism-in-the-public-square-and-its-relevance-to-judaism/index.html">Archiv</a></li>
</ol>