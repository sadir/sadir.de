---
layout: post.njk
title: Denkfehler unter Spirituellen
date: 2021-02-07T06:35
---
Es gibt gewisse Denkfehler, die fast alle spirituellen Menschen teilen – und mit spirituell meine ich hier mehr die New-Age- und Neugeist-Bewegung, also Denkrichtungen, die etwa ab dem 20. Jahrhunderts populär geworden sind und nicht die klassische religiöse Spiritualität. Darunter wären

## 1. Verleugnung des Materiellen

Ihr Grundsatz ist nicht, dass wir materielle Wesen sind, die spirituelle Erfahrungen machen können, sondern dass wir spirituelle Wesen sind, die materielle Erfahrungen machen. Dementsprechend finden sie für alle weltlichen Phänomene, denen wir unterworfen sind, eine geistige Erklärung. Egal um welche Probleme es geht, zu den üblichen Erklärungsmethoden gibt es immer auch eine geistige Komponente. So dass man zu allem als Antwort positives Denken, Meditation usw. bekommt.

Dass wir aber in erster Linie biologische Wesen sind, wird völlig außer Acht gelasst. Es wird nicht beachtet, dass sie höchstens ein paar Stunden, nachdem sie einem geistige Lösungen für ein materielles Problem gegeben haben, wieder etwas essen müssen; und später am Abend müde werden und schlafen müssen – also biologischen Zyklen unterworfen sind. Über solche alltäglichen biologischen Rhythmen wird aber nie gesprochen, nur über Anomalien wie Krankheiten, obwohl diese auf den gleichen Grundlagen im exakt gleichen Körper beruhen.

Diese Herangehensweise beruht auf mehreren Thesen, die ihrem Weltbild zugrunde liegen. Darunter fallen 1. "Gedanken beeinflussen Materie" und

## 2. "Alles ist eins"

Ich will nicht behaupten, dass dies nicht auf irgendeiner Ebene wahr ist; mir geht es viel mehr darum, auf welch falsche Weisen das aufgefasst wird und zu welchen irrsinnigen Schlüssen Menschen deswegen kommen.

Die Behauptung, dass es ein Universales Bewusstsein gibt, das das gesamte Universum durchdringt und so uns auf irgendeine Weise miteinander verbindet, lässt viele spirituelle Menschen Dinge denken wie "Dieses Universale Bewusstsein, das man auch göttliches Prinzip nennt, durchdringt alles, auch mich, also bin ich auch Gott" und "Wenn wir alle dadurch verbunden sind, gibt es keinen Unterschied zwischen mir und dir, wir sind beide eins". Was beides natürlich total **FALSCH** ist. Es gibt sehr wohl einen Unterschied zwischen Menschen und wir sind auch alles komplett getrennte Wesen. Dennoch ist die Theorie des Universalen Bewusstseins nicht wirklich abstrus. Wieso viele aber die Existenz eines alles durchdringenden Geistes und die Trennung der Menschen nicht vereinbaren können, liegt am Mangel der Kenntnis von Dimensionen.

Wenn wir miteinander verbunden sind, dann in einer höheren Dimension. Auf der räumlichen – dreidimensionalen Ebene – sind wir voneinander getrennt. Das kann man ganz einfach veranschaulichen, indem man ein Blatt Papier nimmt und zwei Punkte darauf macht. Die Punkte sind verbunden auf der zweidimensionalen Ebene, da sie Teil des gleichen Blattes sind, aber auf der eindimensionalen Ebene sind sie getrennt voneinander. Genauso sind wir räumlich von anderen Menschen getrennt und bei all den sozialen, biologischen, physikalischen Gesetzen, denen wir unterworfen sind, spielen die höheren Dimensionen, in denen wir potentiell verbunden sind, keine Rolle, da all diese Gesetze innerhalb von drei, maximal vier, Dimensionen funktionieren.

Ich habe schon Versuche gesehen, dieses Universale Bewusstsein irgendwie zu modellieren oder zumindest greifbar zu machen. Diese Modelle werden aber natürlich immer verzerrte Resultate zustande bringen, da uns schlichtweg die Vorstellungskraft über höhere Dimensionen fehlt. Um das zu veranschaulichen, benutze ich dafür immer gerne die Schattenanalogie:

Nehmen wir dich als dreidimensionales Wesen und schauen uns deinen Schatten an, der zweidimensional ist. Nehmen wir an, es gäbe jemanden, der nicht dazu in der Lage ist dreidimensionale Objekte zu sehen, sondern nur die Schatten dieser. Wieviel könnte er über dich sagen nur aufgrund des Schattens? Wie genau wäre sein Bild von dir? Wieviel an Information würde ihm fehlen, weil er blind für eine Dimension ist?

Nehmen wir jetzt an, Schatten könnten Schatten werfen. Sie wären demnach eindimensional – also ein Strich. Wenn jetzt jemand blind für die zweite und dritte Dimension wäre und nur eindimensional sehen könnte, würde alles wie Striche für ihn aussehen. Wieviel Information geht von deinem Schatten, geschweige denn von deinem dreidimensionalen Körper verloren?

Wenn man den Gedanken weiterspinnt, sind wir nur Schatten einer noch höheren Dimension. Wieviel kann ich über dein fünf- oder sechsdimensionales Ich sagen, wenn ich deinen dreidimensionalen Körper angucke? Die Menge an Information, die mir über eine höhere Dimension fehlt, ist so unvorstellbar hoch, wenn man bedenkt, dass es so scheint, als ob der Informationsverlust von Dimension zu Dimension nicht linear, sondern exponentiell ist, denn ein Schatten sagt unverhältnismäßig mehr über ein Objekt aus als ein Strich.

Was ist, wenn wir auf der siebten Dimension miteinander verbunden sind? Spielt es wirklich eine Rolle für uns, die wir Schatten eines Schattens eines Schattens usw. sind? Wie groß kann der Einfluss auf uns sein und ist er wirklich größer als die materiellen Gegebenheiten, denen wir unterworfen sind? Und falls doch ein merklicher Einfluss da ist und es Menschen gibt, die von praktischen Erfahrungen erzählen können, dann steht zumindest fest, dass diese es nicht auf stichhalte Weise werden erklären können. Spiritualität sollte immer Praxis sein. Ausschließlich Praxis. Jeder Versuch es in Worte zu fassen, wird es ad absurdum führen.