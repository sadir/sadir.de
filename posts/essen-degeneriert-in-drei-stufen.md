---
layout: post.njk
title: Essen degeneriert in drei Stufen
date: 2023-01-08T13:35
---
## Sushi
1. Leute verstehen darunter rohen Fisch. Zur Fermentierung wird er in gekochtem Reis eingerollt, vor dem Verzehr wird der Reis aber weggeworfen, da er nicht zur Speise gehört.
2. Der Reis wird nicht mehr weggeworfen, sondern mitgegessen.
3. Der Reis wird immer noch mitgegessen, diesmal fällt aber der Fisch weg und wird stattdessen mit Gemüse ersetzt.

## Kebap
1. Fleisch wird an einem vertikalen Drehspieß gegrillt und auf einem Teller serviert.
2. Fleisch wird an einem vertikalen Drehspieß gegrillt, aber diesmal in einem Fladenbrot serviert.
3. Leute bestellen einen Veggie Döner, also nur das Brot ohne das Fleisch.