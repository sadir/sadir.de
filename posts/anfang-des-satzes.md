---
layout: post.njk
title: Anfang des Satzes
date: 2020-11-13T02:45
---
Vor einiger Zeit hatte ich wieder einen "hiding in plain sight"-Moment, als mir die offensichtliche Tatsache begegnet ist, dass man am Anfang des Satzes aus Lesbarkeitsgründen groß schreibt. Dass es ungefähr das gleiche ist, wie ganz am Anfang des Textes den ersten Buchstaben über mehrere Zeilen groß zu machen (wie auch hier bei dem Post).

![Initialen](https://files.sadir.me/posts/20201113-initialen.jpg)