---
layout: post.njk
title: Ein Hash ist kein Hashtag
date: 2020-08-05T02:05
---
Es ist immer wieder zu beobachten, dass Leute eine Raute (oder englisch Hash) als Hashtag bezeichnen. Das ist auch eine recht lustige Verwechslung, wenn man sich überlegt, wie das eigentlich kommt.

Das "Hashtag", wie man es z.B. aus Twitter kennt, war im Prinzip eine neue Art wie man mit Tags/Schlagworten umgeht. Ursprünglich dienten Tags dazu, den Content zu kategorisieren und damit leichter nach ähnlichen Inhalten zu suchen. Sie waren ein integrales Feature der Seite und als solches erkennbar. D.h., man konnte erkennen, dass der Content für sich allein steht und Tags mehr eine Metainformation sind. Twitter hat das ganze geändert, indem die Tags **im** Content sind. Der User konnte also die Kategorien, zu denen sein Post gehört, im Post selbst integrieren.

Das hatte die interessante Konsequenz, dass Leute jetzt direkt mit den Tags konfrontiert wurde, statt wie bei anderes Seiten, wo sie einen extra Bereich hatten oder sogar vielleicht erst sichtbar wurden, wenn man die Beschreibung aufklappt o.ä. Das führte dann also auch dazu, dass sie mitgelesen wurden. Und da beginnt auch schon die Verwirrung. Denn um zu betonen, dass bspw. "#computer" ein Tag – oder in dem Fall ein Hashtag – ist, liest man "Hashtag Computer" und meint damit "Jetzt kommt ein Hashtag: Computer".  Gleichzeitig entsteht aber die Doppeldeutung, denn man könnte meinen, das "Hashtag" in "Hashtag Computer" bezieht sich nur auf "#" und man liest normal von links nach rechts.

Ich frage mich, ob es irgendwann so weit sein wird, dass die Wörterbücher Hashtag als Synonym für Raute/Hash aufnehmen werden. Wäre ja nicht das erste Mal, dass sie sich falscher Verwendung beugen.