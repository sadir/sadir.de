---
layout: post.njk
title: Der Mär von sich selbst genug sein
date: 2022-07-31T20:10
---
Einer der häufigsten Ratschläge, die man zur Selbstfindung oder nach einer Trennung oder einem anderen einschneidenden Ereignis bekommt, ist eine Form von Abgeschiedenheit, das Verbringen der Zeit mit sich selbst. Denn nur wenn man sich selbst genug ist, ist man sozusagen geheilt und kann wieder Kontakt zu anderen Menschen suchen.

Die gleichen Leute, die soetwas sagen, würden aber nicht Zeit in Einzelhaft verbringen. Obwohl das die ultimative Form von Alleinsein ist.

Doch was sie meinen ist, dass man nur keinen direkten Kontakt zu anderen Menschen haben sollte, man sich jedoch trotzdem anderweitig ablenken kann. Viele von ihnen verbringen ihre Zeit im Internet und unterhalten sich sogar dort mit anderen. Andere schauen Filme und hören Musik. Andere lesen Bücher. All das sind soziale Kontakte; indirekte, aber trotzdem soziale Interaktion, denn man setzt sich den Gedanken und der Arbeit von anderen Menschen aus.

Wirklich allein sein würde bedeuten, dass man sich mit gar keinen unterhält, ganz egal in welcher Form und das man auch absolut nichts konsumiert, das Menschen produziert haben. Also so etwas wie Meditation, Waldspaziergänge usw. Doch wie lange kann man all das machen, bis man sich wieder jemanden herbeiwünscht? Jeder, der das versucht und keine starke religiöse Motivation hat es durchzuziehen, wird sehr schnell zu spüren bekommen, dass Menschen soziale Wesen sind und niemand sich selbst genug ist und das Unterfangen alsbald abbrechen.