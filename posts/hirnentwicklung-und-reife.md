---
layout: post.njk
title: Hirnentwicklung und Reife
date: 2021-06-25T03:30
---
Reife und Alter gehen unserer Beobachtung nach Hand in Hand, deswegen wird ein kausaler Zusammenhang zwischen Alter und Hirnentwicklung hergestellt. Doch das Alter – eine Zahl – beeinflusst ja nicht deine Reife. Wenn du im Kindesalter in einen Raum gesperrt wirst und nie wieder neuen Dingen ausgesezt wirst, wird als Erwachsener dein geistiges Alter dem deiner Altersgenossen entsprechen? Wohl kaum.

Man weiß, dass das Gehirn sich nie aufhört zu entwickeln und der Grund, wieso er in früheren Stadien größere Strecken zurücklegt, wird der sein, dass sich da die Umstände auch auf tiefgreifendere Art verändern. Nicht nur lernt man im jungen Alter öfter und mehr neues, man tritt auch öfter in neue Abschnitte. Der erste Job ist ein größerer Einschnitt, als der vierte Jobwechsel. Zum ersten Mal in die Schule, zum ersten Mal die Schule beenden, zum ersten Mal die eigene Wohnung, das erste Kind etc. Jedes dieser Veränderungen hat ein größere Wirkung auf das Gehirn. Deswegen haben junge Leute beim Rückblick öfter als ältere Menschen das Gefühl, dass sie vor einigen Jahren unreif waren. Denn je größer der Wandel von dem einen Stadium zum anderen, desto mehr wird dir dein altes Ich unreif oder naiv vorkommen.

Fragt man beispielsweise einen 50-Jährigen, der eine größere Veränderung gemacht hat, wie einen neuen Glauben annehmen, wird er im Rückblick sein 45-jähriges Ich als noch naiv oder unreif bezeichnen. Aber war er das wirklich? Nein. Es ist eine Illusion, die bei der Entwicklung des Gehirns entsteht. Und diese Entwicklung tritt in der Regel am häufigsten im jungen Alter auf, sie hat aber keine inhärente Verbindung zum Alter. Diese falsche Assoziation nenne ich deshalb **Alter-Reife-Fehlschluss**.