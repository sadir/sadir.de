---
layout: post.njk
title: Meinungswirt
date: 2022-04-20T22:40
---
Manchmal diskutiert man mit jemanden, der wirklich Ahnung von dem hat, was er sagt; oder zumindest gute Gründe nennen kann, wieso er denkt, was er denkt. Aber immer wieder stößt man auch auf Leute, die sehr überzeugt sind von ihrer eigenen Meinung, aber wenn man anfängt etwas zu graben, stellt man fest, dass nicht viel Substanz dahintersteckt. Trotz dieser Demonstration werden diese Leute aber in ihrer Position nicht erschüttert; auch wenn sie selbst feststellen, dass sie eigentlich nicht viel zu sagen haben, nehmen sie für sich nach wie vor die überlegene Position in Anspruch. Aber warum ist das so?

Der Grund ist, dass viele Menschen ihre Meinung outsourcen – und zwar an Leute, die sie als Autoritäten in diesem Gebiet erachten. Es findet hierbei aber keine klassiche Lehrer-Schüler-Beziehung statt, in der man versucht von dieser Autorität zu lernen, um eines Tages auf dem gleichen Stand zu sein oder sie vielleicht sogar zu übertrumpfen, sondern es ist eine vollständig vertrauensbasierte Beziehung, die zu ändern auch nicht beabsichtigt wird.

Nun ist es so, dass wenn dieser "Schüler" in eine Diskussion gerät, er nicht etwa sich aufrichtig auf die Argumente der Gegenseite fokussiert, sondern von vornherein weiß, dass selbst wenn er die Diskussion verlieren würde, er dennoch richtig liegt, denn wäre seine Autorität an seiner Stelle, hätte sie ganz sicher gewonnen. Diese Autorität ist sozusagen ein **Meinungswirt** und erlaubt es ihm sich priviligiert zu fühlen und auf die Gegenseite herunterzublicken, selbst wenn er gar kein Recht dazu hat und gar nichts geleistet hat.

Der "Schüler" – oder besser gesagt **Meinungsgast** – bemerkt aber dabei nicht, dass *er selbst* keine Ahnung von diesem Thema hat, sondern sich blind auf den Meinungswirt verlässt, der ihn mit seinen rhetorischen Fähigkeiten oder Titeln oder seiner Beliebtheit oder aus welchem Grund auch immer überzeugt hat.

Dieses psychologische Phänomen findet aber nicht nur bei fachlichen Themen statt, sondern auch der christliche Slogan W.W.J.D. (What would Jesus do?) basiert zu einem gewissen Maße darauf. Auch Gruppenzwang ist Prinzip nichts anderes, denn jede Gruppe hat einen Kopf, der als Wirt agiert und die anderen in eine Richtung weist. Wenn z.B. eine Gruppe von Schülern einen andern Schüler mobbt, dann geht dieses Mobbing oftmals nur von einem einzigen aus und die anderen (die Gäste) machen mit, weil sie sich nur fragen: W.W.M.D. (What would the Meinungswirt do?).