---
layout: post.njk
title: Glücklichsein
date: 2020-10-29T20:20
---
Jedes Jahr werden Rankings der glücklichsten Länder der Welt gemacht und die Vorderplatzierten werden als Orientierung genommen, um zu schauen was sie richtig oder anders machen. Dabei wird aber nicht unterschieden, ob es sich um wirkliches Glücklichsein handelt oder um die Illusion des Glücklichseins. Denn glücklich kann man auf zwei Arten sein: entweder indem man alles hat, was man braucht oder indem man nicht weiß, dass man nicht alles hat, was man braucht.

Das wahre Fundament auf dem Glück aufbaut ist Freiheit, ist ein freies Leben. Viele in entwickelten Ländern denken, dass sie diese Freiheit haben, besonders im Vergleich zu sogenannten rückständigen Ländern. Ob es wirklich stimmt, kann man mit einer einfachen Frage herausfinden: hast du etwas für diese Freiheit getan, hast du dich dafür eingesetzt, sie zu erhalten? Wenn nein, dann hast du keine. So einfach ist das, Freiheit ist nichts, was dir jemand geben kann.

Kein Staat der Welt, kein Gesetz und keine Institution kann dir ein freies Leben aufdrücken, wenn du im Kopf ein Gefangener bist. Freiheit ist, wenn du all die genannten Dinge überwunden hast. Wenn dich kein Gesetz der Welt aufhalten kann das zu tun, was du für richtig hältst. Das bedeutet nicht, dass du Gesetze brechen solltest des Gesetzebrechens willen, sondern dass Freiheit bedeutet, in einer Position zu sein, über Gesetze springen zu können, wenn sie einem gerade im Weg stehen.

