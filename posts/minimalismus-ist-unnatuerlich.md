---
layout: post.njk
title: Minimalismus ist unnatürlich
date: 2021-02-07T11:35
---
Es ist eines der Prinzipien des Universums, dass es alles im Überfluss gibt. Die Natur spart nicht. Jeder, der sein Besitz herunterbricht und versucht alles so minimal wie möglich zu halten, handelt demnach wider der Natur. Auf großem Fuß zu leben bedeutet im Einklang mit der Natur zu sein.

Tatsächlich ist das Überflussprinzip sogar eine sehr wichtige Säule der Evolution: je mehr produziert wird, desto weniger fallen die Misserfolge ins Gewicht und desto eher können sich Erfolge herauskristallisieren. Deswegen bezieht sich dieser Post auch nicht gegen die Minimalismus-Praktik, seinen Kleiderschrank auszuräumen und alles zu entsorgen, was man lange nicht mehr angezogen hat. Denn das bedeutet nur alten Ballast loszuwerden, der den Test der Zeit nicht bestanden hat und ist demnach im Einklang mit der Natur.