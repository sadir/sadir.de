---
layout: post.njk
title: Jahreszeitwechsel
date: 2020-10-11T02:55
---
Ganz gleich, wie oft man es schon mitgemacht hat, aber es ist doch immer wieder ein seltsames Gefühl, wenn man zum ersten Mal im Jahr eine Jacke anziehen muss oder wenn man zum ersten Mal im Jahr kurzärmlig rausgehen kann.