---
layout: post.njk
title: Rache an den Mörder des eigenen Sohnes
date: 2021-05-29T00:30
---
In seiner Doku "Where to Invade Next" besuchte Michael Moore verschiedene Länder, u.a. Norwegen, [wo er den Zuschauern die Gefängnisse dort vorstellt](https://www.youtube.com/watch?v=0IepJqxRCZY) und zeigt, dass das Gefängnissystem dort auf der Idee der Resozialisierung aufgebaut ist. Dass die sich so sehr von den Gefängnissen anderer Länder unterscheiden, dass man sie nicht einmal als solches erkennt. Die Norweger sehen es aber als ein Erfolg an, denn nur 20% der Gefangenen werden innerhalb der ersten fünf Jahre nach Freikommen wieder rückfällig, im Gegensatz zu den USA, bei der die Quote bei 80% liegt.

Erst könnte man meinen, dass dieser Fakt für sich spricht und man nicht dagegen argumentieren kann, denn immerhin, Zahlen lügen nicht. Doch etwas später führt er ein [Interview mit dem Vater eines der Jungen, die 2011 von Anders Breivik getötet wurden](https://www.youtube.com/watch?v=yUoqtqFkaZ0).

<video controls src="https://files.sadir.me/posts/20210528-breivik-sohn-vater.mp4"></video>

In diesem Interview wird der Vater damit konfrontiert, dass Breivik in Norwegen eine viel leichtere Strafe bekommt, da er eine deutlich schlechtere Zeit in Gefängnissen in anderen Ländern hätte. Der Vater meint daraufhin nur, dass er einen fairen Prozess für ihn begrüßt und es gut findet, wie es ist. Und auf die Frage, ob er ihn nicht am liebsten töten würde, für das, was er mit seinem Sohn gemacht hat, verneint er entschieden und meint, dass er sich nicht auf dieses Level herabbegibt und kein Recht hat, einem anderen Menschen das Leben zu nehmen.

Dieser kurze Ausschnitt hat sofort meine Augen geöffnet. Allein dieser Grad an Programmierung, die hat stattfinden müssen, um so eine Haltung zu haben, ist verblüffend. Rache ist ein völlig natürliches Gefühl in so einer Situation, es ist ein völlig natürliches Verlangen, denjenigen, der Leid über deine Familie gebracht hat, leiden oder sterben sehen zu wollen. Selbst wenn man zu feige ist, um es selbst auszuführen, sollte doch wenigstens die Fantasie, der Wunsch, da sein. Aber nein, es wurde jegliche Intuition ausgelöscht.

Es dürfte kein leichter Prozess sein, so einen tiefsitzenden menschlichen Trieb zu überschreiben, weswegen es darauf hindeutet, dass die Leute in diesem Land von klein auf auf diese Weise programmiert werden. Daher wird er auch nicht der einzige Mensch sein, der so denkt, sondern er spiegelt wahrscheinlich den durchschnittlichen Norweger wieder. Denn selbst wenn er innerlich eigentlich anders denkt, aber vor der Kamera einfach etwas sagt, wovon er glaubt, dass es besser ankommt, ändert es nichts daran, denn allein die Tatsache, dass er denkt, dass so etwas besser bei den Leuten ankommt, sagt genug über den Zustand der Nation aus.

Auch dass er in so einer Situation von sog. "Rechten" redet, die er nicht hat, zeigt wie sein Denken von Staatspropaganda durchzogen ist. Er schafft es nicht einmal einen klaren und natürlichen eigenen Gedanken zu fassen, ohne dass das Ideal einer von oben aufgedrückten Sittlichkeit in seinem Kopf spukt.

Denken wie dieses dürfte der Grund sein, wieso die Rückfallkriminalität in Norwegen geringer ist als in anderen Ländern. Denn Menschen dort scheinen prinzipiell lammfrommer zu sein. Das demonstriert auch ganz gut, wieso man nicht verschiedene Länder aufgrund nur eines Faktors miteinander vergleichen sollte, wenn man nicht auch alle anderen in Betracht gezogen hat, um sicherzustellen, ob nicht in Wirklichkeit nur eine Korrelation vorherrscht.