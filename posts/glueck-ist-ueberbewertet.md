---
layout: post.njk
title: Glück ist überbewertet
date: 2021-07-06T00:30
---
Wenn wir uns zu sehr auf Schicksal oder Zufall verlassen, nimmt es uns unsere Motivation zu handeln. Wir werden wie Blätter im Wind sein, ohnmächtig und komplett den Luftströmen ausgeliefert. Zwar stimmt es, dass die Zukunft ungewiss ist, aber nicht alles, was passiert, muss uns treffen. Wie klug wir heute handeln bestimmt wieviel Glück wir morgen haben.

> *Es gibt Regeln für das Glück: denn für den Klugen ist nicht alles Zufall. Die Bemühung kann dem Glücke nachhelfen. Einige begnügen sich damit, sich wohlgemut an das Tor der Glücksgöttin zu stellen und zu erwarten, daß sie öffne. Andere, schon besser, streben vorwärts und machen ihre kluge Kühnheit geltend, damit sie, auf den Flügeln ihres Wertes und ihrer Tapferkeit, die Göttin erreichen und ihre Gunst gewinnen mögen. Jedoch richtig philosophiert, gibt es keinen andern Weg als den der Tugend und Umsicht; indem Jeder gerade so viel Glück und so viel Unglück hat, als Klugheit oder Unklugheit.*  
> – Baltasar Gracián<sup>[[1]](#fn-1)</sup>

---

<ol class="fns">
    <li id="fn-1">Aus "Handorakel und Kunst der Weltklugkeit" (Übersetzung von Arthur Schopenhauer)</li>
</ol>