---
layout: post.njk
title: Verzicht vs. Ersatz
date: 2021-02-07T07:20
---
Die Goldene Regel der Mechanik lernt jedes Kind im Physikunterricht: wenn man einen kürzeren Weg nehmen will, muss man mehr Kraft aufbringen; will man weniger Kraft einsetzen, muss man einen längeren Weg nehmen – in jedem Fall bleibt die Arbeit die gleiche. Das hat mir damals gelehrt, dass es keine Abkürzungen im Leben gibt und es gleich physikalisch bewiesen.

Seitdem sehe ich jedes Problem und jede Lösung durch die Linse der goldenen Regel. Immer wenn ich eine superinnovative Idee sehe, die alles bisher dagewesene toppt, weiß ich, dass an irgendeinem anderen Ende gespart wurde, sodass das Gesamtresultat – wenn es eine Formel gäbe, die alle Variablen berücksichtigt – das gleiche ist. Es gibt keine Medizin ohne Nebenwirkungen.

Und obwohl das so selbstverständlich ist, wird es von den meisten Leuten bei Diskussionen kaum beachtet. Wenn es beispielsweise um die Klimaerwärmung und den Umstieg auf erneuerbare Energien geht, sehe ich die Verfechter fast nie sagen, dass es keine magischen Wundermittel sind, sondern ihre ganz eigenen Nachteile mit sich bringen, sodass das Gesamtresultat das gleiche bleibt. Genauso auch mit Elektroautos.

Niemand würde sagen, dass der Umstieg von Zigaretten auf E-Zigaretten besser ist, als ganz damit aufzuhören. Und auch wenn der komplette Verzicht in dem Moment vielleicht unrealistisch ist, würde man wenigstens erwähnen, dass es zumindest Ursache des Problems ist und solange diese Ursache da ist, wird jeder Ersatz andere Nachteile mit sich bringen, die genauso unerwünscht sind.

Es gibt Versuche einzelner auf gewisse Dinge zu verzichten, wie z.B. den Flugverkehr und auch Verbote oder zumindest Regulationen in der Richtung anzustrengen. Das Problem dabei ist aber, dass das nur Symptome sind. Flugzeuge sind nicht das Problem, sondern die industrielle Gesellschaft, zu der Flugzeuge zwangsläufig dazugehören. Selbst wenn man alle Flüge generell verbieten würde und kein einziges Flugzeug mehr abheben darf, wäre das nicht von langer Dauer.

Man muss sich jedes System wie ein Puzzle vorstellen und jedes Puzzlestück ist ein Element, das dieses System bildet. Zu einer industriellen Gesellschaft gehören Flugzeuge dazu. Würde man dieses einzelne Stück rausnehmen, aber das Bild als solches lassen, würden sich sehr viele Menschen darüber empören und alle Kraft aufwenden, um es wieder rückgängig zu machen. Man kann es auch niemanden verübeln, denn wieso sollten sie in einem unvollständigen Bild leben wollen? In einem System mit Löchern? Und diese Gegenbewegung würde so laut sein und so lange gehen, bis das Puzzlestück wieder an seinem Platz und das Bild vollständig ist.

Aber was ist das für ein Bild? Wer bestimmt, was darauf abgebildet ist? Kein einzelner bestimmt es, es wird festgelegt von Revolutionen – und ich meine damit umwälzende Entwicklungen und nicht umstürzende politische Bewegungen, die an Utopien glauben. Entwicklungen wie die landwirtschaftliche oder industrielle Revolution. Oder auch die Globalisierung, die ein Kind der industriellen Revolution ist und in dessen Bild wir aktuell leben. Jeder Versuch die Nebenwirkungen dieses Bildes zu bekämpfen, ohne zu versuchen das Bild als ganzes zu wechseln, ist nur heiße Luft und wird langfristig nirgendwohin führen. Das ist eine wissenschaftliche Tatsache.