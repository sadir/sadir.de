---
layout: post.njk
title: Die Erfindung des morgen
date: 2021-04-22T19:00
---
Wenn wir über Zeit nachdenken, stellen wir sie uns oft vor wie eine Linie, ähnlich eines Zahlenstrahls, verlaufend in eine Richtung. Wir sind in der Mitte, das ist die Gegenwart und der Strahl verläuft durch uns; die Linie vor unseren Augen ist die Zukunft, die Linie hinter uns die Vergangenheit.

Doch wäre die Zukunft tatsächlich vor uns, müssten wir sie sehen können und wäre die Vergangenheit hinter uns, dürften wir sie nicht sehen. Jedoch ist es in Wirklichkeit genau umgekehrt – die Vergangenheit liegt vor und die Zukunft hinter uns und wir laufen rückwärts ohne zu wissen, worauf wir stoßen werden. Man könnte also meinen, der Zeitstrahl wäre genau umgekehrt und wir leben nicht nach vorne, sondern zurück.

## Blick in die Vergangenheit

Doch der Schein trügt, denn wir können nicht wirklich in die Vergangenheit sehen, das einzige was wir haben, sind unsere Erinnerungen. Doch sind Erinnerungen nicht nur vergessbar oder anfällig für Verzerrungen, sondern sie sind auch zukünftigen Uminterpretationen ausgesetzt. Wie oft hat man nicht nach veränderten Lebensumständen oder neuen Dingen, die man gelernt hat, zurück geblickt und gewisse Gegebenheiten plötzlich aus anderen Augen gesehen? Wie oft hat man nicht sein eigenes Leben retrospektiv verändert?

Erinnerungen sind nur Modelle des Erlebten. Sie sind Gemälde, die unsere Gehirn zeichnet – sie bilden die Realität ab, aber sie sind nicht die Realität. Manche der Gemälde sind fotorealistisch, manche expressionistisch, manche abstrakt, alle sind sie Interpretationen ausgesetzt und geben uns ein Bild der Vergangenheit, jedoch ein lückenhaftes.

Schnell wird klar, einen Zeitstrahl gibt es nicht wirklich. Denn wenn es weder eine Zukunft gibt, die bestimmt ist und die Vergangenheit nur eine Illusion ist, die durch unsere Erinnerungen gebildet wird, dann kann Zeit nicht durch eine Linie dargestellt werden. Es gibt nur einen einzigen Moment, das Jetzt und der Zustand des Jetztes verändert sich stetig von Jetzt auf Jetzt. Alles, die gesamte Weltgeschichte, ist ein Moment.

## Einfluss von Zyklen

Würden wir aber die Zeit als das betrachten, was sie ist, könnten wir nicht damit arbeiten. Wir könnten keine Ordnung und keine Zivilisationen aufbauen. Dadurch, dass sich in der Natur die Zustände nicht immer ganz willkürlich verändern, sondern gewissen Trends folgen oder immer wiederkehrende Ereignisse stattfinden, nutzte der Mensch diese Beobachtung aus, um nach *divide et impera* ein Modell zur Strukturierung zu erstellen.

Das kann bei simplen Trennungen anfangen, wie bspw. zu sagen, dass ein neuer Tag beginnt, wenn die Sonne aufgeht oder eine neue Jahreszeit beginnt, wenn sich die klimatischen Bedingungen verändern, bis zu Trennungen, die man macht durch längerfristigere Beobachtungen der Himmelskörper.

Jedoch darf man nicht vergessen, dass Stunden, Tage, Monate, Jahre usw. nur Maßeinheiten sind und sie nicht *wirklich* existieren. Sie existieren nur genauso, wie auch Meter, Kilogramm oder Pfund existieren. Menschen haben sie erschaffen zur Kommunikation und um damit arbeiten zu können, aber es gibt in Wirklichkeit keinen Unterschied zwischen gestern und heute, außer, dass es zwischendurch dunkel war. Denn würden wir das ganze aus der Vogelperspektive betrachten – als ein Beobachter aus dem Weltall – würden wir nur sehen, wie sich die Erde um sich selbst dreht und sich sonst nichts verändert hat und auch kein neuer Abschnitt beginnt.

## Zeit als physikalische Einheit

Den einzigen Moment, den es gibt, in Abschnitte zu trennen, hilft nicht nur bei der Planung, sondern auch dabei Dinge vorherzusagen, also sie zu berechnen. Somit wurde Zeit zu einem wissenschaftlichen Modell.<sup>[[1]](#fn-1)</sup> Wissen wir bspw. die Geschwindigkeit eines Objektes, können wir sagen, in welcher Zeit er eine gewisse Strecke zurücklegen wird. Durch diese Berechenbarkeit wurde Zeit plastisch für viele Menschen, sie wurde real.

Doch dürfen wir niemals vergessen, dass Berechenbarkeit überhaupt einer der Hauptgründe für die Erstellung von Modellen ist. Wir haben einen festgesteckten Rahmen innerhalb dessen wir agieren und innerhalb dieses Rahmens formulieren wir durch Beobachtung gewisse Gesetzmäßigkeiten und mithilfe dieser Gesetzmäßigkeiten können wir vorhersagen, welche Beobachtungen wir in Zukunft machen werden – das ist der ganze Sinn hinter Wissenschaft. Doch da Modelle immer nur die Wirklichkeit abbilden und nicht die Wirklichkeit selbst sind, wird man mit ihr auch immer an seine Grenzen stoßen, z.B. wenn man außerhalb des festgesteckten Rahmens schaut. Seit der speziellen Relativitätstheorie wissen wir, dass sich die Zeit nahe der Lichtgeschwindigkeit langsamer bewegt; dass solche unintuitiven und nur mathematisch sinnvollen Beobachtungen machen lassen, liegt allein daran, dass unser Modell der Zeit falsch ist. Genauso wie die Tatsache, dass sich Teilchen auf Quaten-Ebene für uns seltsam verhalten, nicht an der Willkür des Universums liegt, sondern daran, dass unser Atommodell falsch ist.

## Unsere Intuition

Wenn wir unser eigenes Verhalten und unser Denken beobachten, werden wir feststellen, dass wir viele Situationen so handhaben, als gäbe es keine Zeit. Daniel Kahneman schreibt<sup>[[2]](#fn-2)</sup> dazu über sein Erlebnis bei Verdis Oper *La Traviata*:

>*Bekannt für ihre hin­rei­ßen­de Musik, ist sie auch eine be­we­gen­de­ Lie­bes­ge­schich­te zwischen einem jungen Adligen und Violetta, einer Frau aus der Halbwelt. Der Vater des jungen Mannes sucht Violetta auf und überredet sie dazu, ihren Geliebten auf­zu­ge­ben, um die Ehre der ­Fa­mi­lie und die Hei­rats­chan­cen der Schwester des jungen Mannes zu schützen. In einem Akt höchs­ter­ Selbst­auf­op­fe­rung gibt Violetta vor, auf den Mann, den sie über alles liebt, ver­zich­ten zu wollen. Bald ­dar­auf flammt ihre Schwind­sucht (die im 19. Jahr­hun­dert ge­bräuch­li­che Be­zeich­nung für Tu­ber­ku­lo­se) auf. Im letzten Akt liegt Violetta auf dem Ster­be­bett, umgeben von ein paar Freunden. Als ihr Ge­lieb­ter­ von ihrem Zustand erfährt, eilt er nach Paris, um sie zu sehen. Sobald sie die Nachricht hört, flößen ih­r Hoff­nung und Freude neuen Lebensmut ein; trotzdem ver­schlech­tert sich ihr Zustand rapide.*<br><br>
*Ganz egal, wie oft man die Oper gesehen hat, packen einen immer wieder die Spannung und die Furcht des Au­gen­blicks: Wird der junge Liebhaber recht­zei­tig ein­tref­fen? Man spürt, dass es für ihn enorm­ wich­tig ist, seine Geliebte noch einmal zu sehen, bevor sie stirbt. Natürlich kommt er recht­zei­tig; ei­ni­ge ­wun­der­ba­re Lie­bes­du­et­te werden gesungen, und nach zehn­mi­nü­ti­gem herr­li­chem Gesang segnet Vio­letta ­das Zeitliche.*<br><br>
*Auf dem Nach­hau­se­weg von der Oper fragte ich mich: Weshalb sind uns diese zehn Minuten so wichtig? Ich erkannte sehr schnell, dass mir die Länge von Violettas Leben völlig egal war. Wenn man mir gesagt hätte, dass sie mit 27 Jahren starb, nicht mit 28, wie ich glaubte, hätte mich die Nachricht, dass sie ein Jahr Le­bens­glück verpasst hatte, überhaupt nicht berührt. Aber die Mög­lich­keit, die letzten zehn ­Mi­nu­ten zu versäumen, war von großer Bedeutung. Außerdem hätten sich die Gefühle, die die ­Wie­der­ver­ei­ni­gung der Geliebten in mir auslösten, nicht verändert, wenn ich erfahren hätte, dass sie statt zehn Minuten eine Woche mit­ein­an­der verbracht hätten. Doch wenn der Liebhaber zu spät ge­kom­men­ wä­re, wäre La Traviata eine ganz andere Ge­schich­te gewesen. Eine Ge­schich­te dreht sich um be­deu­ten­de ­Er­eig­nis­se und denk­wür­di­ge Momente, nicht um das Vergehen der Zeit. In einer Ge­schich­te ist die ­Ver­nach­läs­si­gung der Dauer normal, und der Schluss definiert oftmals ihren Charakter.*

Dieses Phänomen nennt sich in der Psychologie Höchststand-Ende-Regel<sup>[[3]](#fn-3)</sup> und ist auch der Grund dafür, dass Filme mit Happy-End so beliebt sind, denn was zählt ist immer der Moment, der am Ende dabei herauskommt, ganz egal wie lange es gedauert hat dahinzukommen. Zeit spielt bei unserer Beurteilung darüber keine Rolle, nur das Erlebte. Gerade diese unbewussten Prozesse, die in unserer Natur liegen und weder durch menschliche Lehren noch durch Sozialisation verändert wurden, zeigen uns ganz deutlich das Wesen der Welt. Durch sie können wir erkennen, ob eine Sache nur menschengemacht ist und uns nur real vorkommt, weil wir darauf konditioniert wurden oder ob sie wirklich existiert – im Falle der Zeit ist es demnach ersteres.

---

<ol class="fns">
    <li id="fn-1">Mehr zu Modellen habe ich in "<a href="/posts/die-missdeutung-von-wissen/">Die Missdeutung von Wissen</a>" geschrieben</li>
    <li id="fn-2">"Schnelles Denken, langsames Denken", Kapitel 36.</li>
    <li id="fn-3">Weitere Beispiele dazu auch in Kapitel 35.</li>
</ol>