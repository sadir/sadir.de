---
layout: post.njk
title: Denken ist kein Handeln
date: 2021-07-03T21:50
---
Ich habe schon mehrmals über die Denkmuster moderner Spiritualität geschrieben<sup>[[1]](#fn-1)</sup>, hier möchte ich aber näher auf eines der weitverbreiteststen Ideen eingehen: nämlich der Ansicht, dass Gedanken Materie beeinflussen. Dass durch bloßes Denken sich das Universum in Gang setzt und einem das holt, was man sich wünscht. Dass die Wahrscheinlichkeit, dass man es bekommt, dadurch erhöht wird, dass man länger und intensiver daran denkt, bedeutet: mit mehr Überzeugung und Emotion. Kurz gesagt, das Gesetz der Anziehung.

Aber was ist da dran? Ich kann zum Beispiel denken, dass ich meinen linken Arm heben will und mache es daraufhin – heißt es nicht, dass mein Gedanke es ausgelöst hat? Mein Arm besteht aus Materie und bevor er seine Position verändert hat, habe ich erst daran gedacht. Ich kann daran denken, dass ich den Bildschirm vor mir zerstören will; dann nehme ich einen Hammer in die Hand und schlage auf ihn ein. Kann ich nicht behaupten, dass mein Gedanke der Auslöser war? Dass er es war, der die Veränderung in dieser physischen Welt ausgelöst hat?

Hier muss man vorsichtig verfahren, denn man kann leicht zu Fehlschlüssen kommen. Ein Gedanke ist nichts anderes als Energie, sie entsteht durch elektrochemische Reize, ausgelöst von Nervenzellen. Wenn einer der Gedanken ein direkter Befehl an den Körper ist, wandelt der Körper diese Energie in eine andere Form der Energie um bzw. nutzt die bereits in deinen Zellen gespeicherte Energie durch Verstoffwechslung, damit sie dir zum Bewegen deines Armes zur Verfügung steht. Die Gedanken haben also direkten Einfluss auf deinen Körper, aber nicht auf die Umwelt. Dein Körper wird sozusagen als Werkzeug benutzt, *um* Einfluss auf die Umwelt zu bekommen. Man kann sich den Gedanken also wie einen Piloten und den Körper wie ein Flugzeug vorstellen. Wenn du jetzt einen Hammer in die Hand nimmst, um den Bildschirm zu zerstören, ist der Hammer ein Werkzeug deiner Hand, also ein Werkzeug eines Werkzeugs deines Gedankens.

Der Gedanke an sich kann nichts anrichten. Die Veränderung wird erst durch die Handlung verursacht. Und für diese Handlung brauchst du deinen eigenen physischen Körper; d.h. Materie verändert Materie. Zu sagen, dass Gedanken trotzdem die Materie verändert haben, weil sie ja den ersten Anstoß für deinen Körper gegeben haben, ist so wie zu sagen, dass Piloten auf sich gestellt die inhärente Fähigkeit haben zu fliegen, weil sie die Fähigkeit haben, ein Flugzeug zu steuern, das fliegen kann.

Der Handlungsspielraum eines Gedankens hängt vom Handlungsspielraum des Handels ab. Wenn du dich bewusst dazu entscheidest nichts zu tun oder wenn du gar nichts tun kannst, weil du z.B. körperlich dazu nicht in der Lage bist, dann wird dir kein Gedanke etwas bringen. Kein Denken daran, dass du den linken Arm heben willst, wird dir dieses Resultat bringen, wenn du körperlich gelähmt bist. Das Universum bringt dir nichts durch bloßes Denken, deine Taten tun es.

<figure class="right">
    <a href="https://files.sadir.me/posts/20210703-domino.png">
        <img alt="Gedanke, Handlung und Veränderung als Dominosteine" src="https://files.sadir.me/posts/20210703-domino.png">
    </a>
</figure>

Diese Philosophie bringt Menschen dazu die meiste Zeit nur mit Denken und Wünschen zu verbringen und niemals das zu bekommen, was sie wollen. Und wenn sie mal bekommen, was sie wollen, war es nicht von Zufall zu unterscheiden oder das Resultat ihrer eigenen Arbeit. Viele Menschen wünschen sich etwas vom Universum und stecken danach Arbeit rein und wenn die Arbeit Früchte trägt, sagen sie, dass das Universum es ihnen beschert hat. Was nicht falsch ist, aber das Universum hat auf deine Taten reagiert, nicht auf deine Gedanken. Das ist eine wichtige Unterscheidung, denn Menschen, die so denken, werden nicht in allen Aspekten den Gedanken Taten folgen lassen und so werden sie in manchen Fällen denken und wünschen, bis sie alt und grau sind. Diesen Prozess kann man sich wie ein Dominospiel vorstellen. Veränderung folgt auf Handlung folgt auf Gedanke. (s. Abb. 1) Nimmt man die Handlung aus der Reihe raus, dann bleibt die Veränderung unberührt, wenn der Gedanke fällt. (s. Abb. 2)

<figure>
    <a href="https://files.sadir.me/posts/20210703-domino-beidseitig.png">
        <img alt="Gedankenstein kann in beide Richtungen fallen" src="https://files.sadir.me/posts/20210703-domino-beidseitig.png">
    </a>
</figure>

Gedanken sind aber von unschätzbarem Wert, denn ihre Qualität bestimmt den weiteren Kurs unseres Lebens. So wie in Abb. 3 dargestellt, kann der Gedankenstein in eine von beiden Richtungen fallen und damit eine ganz andere Handlung und einen ganz anderen Umstand im Leben bewirken. In Wirklichkeit gibt es aber natürlich nicht nur zwei Richtungen, es gibt eine Millionen verschiedene Arten wie der Stein fallen kann. Die Qualität unseres Geistes bestimmt also darüber wie die Qualität unseres Handelns sein wird und demnach wie von Erfolg gekrönt die Umstände in unserem Leben sein werden. Nichts zu tun bedeutet Stillstand, Stillstand bedeutet Tod. Das Universum setzt sich nicht in Bewegung für dich, wenn du es nicht selber tust. Es belohnt nur die Produktiven und Tapferen, die Tüchtigen und Handelnden.

---

<ol class="fns">
    <li id="fn-1">s. Posts "<a href="/posts/geist-und-gesundheit/">Geist und Gesundheit</a>" und "<a href="/posts/denkfehler-unter-spirituellen/">Denkfehler unter Spirituellen</a>"</li>
</ol>