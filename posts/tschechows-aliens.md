---
layout: post.njk
title: Tschechows Aliens
date: 2021-10-31T06:30
---
Unter Autoren gibt es die bekannte Technik "Tschechows Gewehr", die vom russischen Schriftsteller Anton Tschechow formuliert wurde. Sie besagt, dass man in einer Geschichte keine überflüssigen Elemente vorkommen lassen sollte: "Entfernen Sie alles, was für die Geschichte nicht relevant ist. Wenn Sie im ersten Kapitel sagen, dass ein Gewehr an der Wand hängt, muss es im zweiten oder dritten Kapitel unbedingt abgefeuert werden. Wenn es nicht abgefeuert wird, sollte es nicht dort hängen."

Nach dem gleichen Prinzip wird von gewissen Instanzen auch im echten Leben verfahren. Dinge, die lange in der Popkultur präsent sind, wie bestimmte Krankheiten oder Fantasiewesen wie Aliens, müssen früher oder später tatsächlich in die Realität geholt werden.

Etwas bereits bekanntes zu nehmen, anstatt sich etwas neues auszudenken, hat zusätzlich den Vorteil, dass die Menschen bereits vertraut damit sind und es deshalb eher glauben werden.