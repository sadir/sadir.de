---
layout: post.njk
title: Unter Beobachtung
date: 2020-11-11T03:41
---
Funktionierende Nachrichtendienste und Überwachung der Bürger ist ein wichtiger Arm des Gesetzes. Menschen verhalten sich immer gesitteter, wenn sie wissen, dass sie beobachtet werden. Doch nicht nur die Gewissheit über Beobachtung lässt einen sich anders verhalten, sondern auch die potentielle Beobachtung. Dome-Kameras funktionieren deshalb so gut, weil man nie sicher sein kann, wohin sie gerade ausgerichtet sind. Es besteht immer die Chance, dass man gerade nicht im Sichtfeld ist, aber man weiß es nicht, also macht es in der Realität keinen Unterschied, ob man gefilmt wird oder nicht, man verhält sich immer so, als ob man es wird.

Besonders in Bereichen, die sich mehr der Kontrolle entziehen, wie dem Internet, ist es Staaten wichtig bei den Bürgern den Eindruck zu erwecken, als ob sie bei jedem Klick getrackt werden. Als ob Geheimdienste alle miteinander vernetzt sind und technologisch dem Rest der Welt mindestens 10-20 Jahre voraus sind und schon jetzt in der Lage sind Verschlüsselungsverfahren zu knacken, die heute noch als bombensicher gelten. Es wird nach Außen der Eindruck vermittelt, als ob Leaks von Whistleblowern, die alle paar Jahre Schlagzeilen machen, rufschädigend wären, aber in Wirklichkeit spielt einem das natürlich in die Karten. Denn es bedeutet für alle: Passt auf Leute, wir sehen alles!

Da wir als Menschen soziale Wesen sind, sind zwei Augen, die auf uns gerichtet sind, ein wichtiger Faktor, der uns in unserem Verhalten beeinflusst. Das funktioniert selbst dann, wenn wir ganz sicher wissen, dass es keine echten Augen sind. Demonstriert wurde das einmal in einem Experiment<sup>[[1]](#fn-1)</sup>, in dem in einer Büroküche, in der die Mitarbeiter immer ihren Kaffee nehmen und auf Vertrauensbasis das Geld in eine Box werfen konnten, ein Bild mit zwei Augen aufgehängt wurde, in die man zwangsläufig gucken musste, wenn man vor der Box stand. An anderen Tagen war irgend ein Bild von Blumen dort. Das Ergebnis war wie folgt:

> *We examined the effect of an image of a pair of eyes on contributions to an honesty box used to collect money for drinks in a university coffee room. People paid nearly three times as much for their drinks when eyes were displayed rather than a control image. This finding provides the first evidence from a naturalistic setting of the importance of cues of being watched, and hence reputational concerns, on human cooperative behaviour.*
> <img class="quote-image" src="https://files.sadir.me/posts/20201111-augen-blumen-experiment.png" alt="Augen erhöhen die Kooperation">

Rein technisch kann man natürlich nicht jeden einzelnen Menschen rund um die Uhr überwachen, aber es wäre nicht so klug vom Staatsapparat nicht wenigstens so zu tun, als ob sie es könnten. Deswegen würde es mich wirklich nicht wundern, wenn dieser Effekt ausgenutzt wird und viele der Meldungen in den Medien über Massenüberwachung gezielt verbreitet werden, so dass sich ja niemand unbeobachtet und sicher fühlt.

---

<ol class="fns">
    <li id="fn-1">Bateson M, Nettle D, Roberts Gilbert. Cues of being watched enhance cooperation in a real-world setting. Biol Lett. 2006 Sep 22; 2(3): 412–414. [<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1686213/">PMC free article</a>]</li>
</ol>