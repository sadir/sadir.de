---
layout: post.njk
title: Zuckersucht überkommen
date: 2020-09-16T06:40
---
Die gesundheitlichen Auswirkungen von Zucker beschäftigen mich schon seit Jahren. Meine Sicht darauf hatte sich komplett geändert, als ich angefangen hatte, ihn als Droge zu sehen. Er hat die gleichen Effekte auf einen, wie auch andere Drogen: beim Konsumieren hat man ein High, danach ebbt es schnell ab und irgendwann später hat man wieder ein starkes Verlangen danach. Und er ist stark gesundheitsschädlich.

Aber auch noch eine andere Eigenschaft hat Zucker mit anderen Drogen gemein, nämlich dass er als Mittel für einen Eskapismus dient. Wir wissen, dass Menschen in schweren Zeiten ihres Lebens eher zur Flasche oder anderen Rauschmitteln greifen; aber auch Frustfressen beginnen. Das sind Phänomene, die uns zeigen, dass unser Körper sehr ungern den Konsequenzen von gewissen Schocks ausgesetzt ist. Weswegen das Gehirn einen dazu bringt, sich selbst abzulenken.  

Während solche großen Ereignisse im Leben ganz offensichtliche Schocks darstellen, könnte man meinen, dass viele Leute zuckersüchtig sind, die in keiner schwierigen Phase sind und es demnach nicht mit Eskapismus erklärt werden kann. Was man aber nicht vergessen sollte ist, dass es nicht nur psychische Schocks gibt, sondern auch biologische. Wenn man bspw. unter einem Nährstoffmangel leidet, ist das auch eine schwierige Situation für den Körper, er muss irgendwie damit umgehen. Er kann nur deinen Appetit auf gewisse Lebensmittel erhöhen, aber ist am Ende auf dich angewiesen, dass du die Signale richtig deutest. Wenn das nicht passiert, muss er sich irgendwie ablenken, also lässt er dich zu Zucker greifen. Und der ist überall um uns herum.

Das ist eine Theorie, die ich durch Beobachtungen an mir selbst erstellt habe. Immer wenn ich ganz gezielt gesund gegessen habe, schwand das Verlangen nach Zucker, egal wie lang der Zeitraum war. Immer, wenn ich eher einseitig gegessen habe und demnach dem Körper ganz offensichtlich gewisse Nährstoffe gefehlt haben, schoss das Verlangen wieder nach oben.