---
layout: post.njk
title: "Rassismus: Wenn Definitionen Verschleierungen sind"
date: 2021-07-29T19:25
---
Das Buch "Might is Right" von Ragnar Redbeard<sup>[[1]](#fn-1)</sup>, bekannt für seine kompromisslose Schreibweise, gilt gemeinhin als eines der extremsten Bücher, die jemals veröffentlicht wurden. In der *Authoritative Edition* des Buches<sup>[[2]](#fn-2)</sup>, die eine Zusammenstellung und Harmonisierung aller Versionen sind, die seit der ersten Ausgabe 1896 erschienen sind, schreibt Trevor Blake, der sich ebendieser dieser Aufgabe viele Jahre lang gewidmet hat: "Ragnar Redbeard speaks for himself. The reader can agree or disagree with what Ragnar Redbeard says in confidence because this *Authoritative* edition offers background information on which to base such a choice. My annotations are generally neutral. Here is an exception. What Redbeard wrote about race makes a strong book ***weak***. It is contradictory and tedious. I say the ego, the *unique*, is superior to all groups, be they of a race, of a sex, of a class or any other grouping."

Viele scheinen diese Meinung zu teilen, denn man liest in Reviews zum Buch öfter über diesen angeblichen Rassismus und in der deutschen Ausgabe<sup>[[3]](#fn-3)</sup> wurden sogar ganze kritische Passagen ausgelassen oder bestimmte Wörter mit anderen ersetzt. (Wenn ich nachfolgend aus dem Buch zitiere, werde ich daher immer zwischen Englisch und Deutsch wechseln – Englisch nehme ich immer, wenn die Passage es nicht in die deutschen Ausgabe geschafft hat.) Auch Anton LaVey, der Begründer der Church of Satan, schreibt in seinem Vorwort: "Ein Bruchteil von *Might is Right* wurde für die Einbindung bearbeitet, weil das Buch dermaßen mit auffallenden Widersprüchen gefüllt war, daß es bestenfalls als Empörungsschrift durchgeht."

Doch was sind das für rassistischen Aussagen, die Menschen so sehr empören? In Kapitel 3.8 schreibt Redbeard:

> *From the soles of his feet to the crown of his head—the bones, skin and flesh of his body—even the grey brain pulp—the electric nerves and tissues—mental ganglia and internal viscera of a man belonging to the African, Mongolian, Semite, or Negrito breeds are all fundamentally different in formation, constituents, and character from the corresponding anatomical sections in men of Aryan descent. The points of non-resemblance may be superficially unperceivable, but they are organic—deep seated.*

> *You have only to look at some men to know that they belong to an inferior breed. Take the Negro, for example. His narrow cranial development, his prognathous jaw, his projecting  lips, his wide nasal aperture, his simian disposition, his want of forethought, originality, and mental capacity are all peculiarities strictly inferior. Similar language may be applied to the Chinaman, the Coolie, the Kanaka, the Jew, and to the rotten-boned city degenerates of Anglo-Saxondom, rich and poor.*

> *What white father for example, would encourage the marriage of a hulking thick skulled Negro with his beautiful and accomplished daughter? Would he enthusiastically "give her away"  to the matrimonial embraces of a Chinaman, a Coolie or the leper-hugs of a polluted "mean white"?*

Die beiden ersten Ausschnitte fehlen in der deutschen Ausgabe komplett, der letzte wurde geändert zu: "Würde ein adliger Vater, zum Beispiel, die Heirat zwischen einem ungebildeten, häßlichen und schwächlichen armen Schlucker und seiner wunderschönen, klugen und feingebildeten Tochter erlauben?"

Doch warum schrieb Redbeard diese Worte? Glaubte er wirklich an eine inhärente Unterlegenheit einer Rasse einer anderen gegenüber? Auf welcher Grundlage fußte er diese Aussagen? In Kapitel 3.10 schreibt er:

> *What the late civil war really accomplished was to degrade the white slave to the lower level of the plantation nigger, and in that respect it was a triumph of ingenuity.*

Dieser Aussage nach können also ein Weißer und ein Schwarzer auf dem gleichen Level sein, nämlich wenn der Weiße ein Sklave ist ("white slave"). Widerspricht das nicht der vorherigen Aussagen, dass Menschen arischer Abstammung per definitionem allen anderen überlegen sind? Wir lesen weiter, in Kapitel 6.9 schreibt er:

> *Seit Anbeginn der Zeit waren die besiegten Klassen stets die Arbeiterklassen, die Knechte, die Sklaven; und die Eroberer und deren Erben haben stets die Priester, Generäle, Aufseher und Herrscher hervorgebracht. Dies gilt ebenso für die Vereinigten Staaten von Amerika, wie für Theben, Troja, Babylon, Persien, Karthago und Rom.*

Hier redet er also über Klassen und nicht über Rassen. Sklavenschaft wird mit dem Besiegtsein in Verbindung gesetzt, nicht mit der Biologie. In Kapitel 5.7 unterstreicht er das noch einmal:

> *Wenn er auf einen wirklichen Widerstand trifft, ist es die Pflicht eines jeden unerschrockenen Menschen, diesen zu überwinden. Sollte das außerhalb seiner Macht liegen – oder der zusammengenommenen Macht seiner Freunde und Unterstützter –, sind Tod oder Unterwerfung die einzig vernünftigen Alternativen. Wenn er nicht getötet wird, müssen er und seine Nachkommen bis in die dritte oder vierte Generation in Unterwerfung leben.*

Der Besiegte – und vor allem die Kinder und Kindeskinder des Besiegten – sind also Sklaven. Nicht "der Schwarze" oder "der Weiße" oder "der Asiate", sondern der Besiegte, ganz egal welcher Hautfarbe er ist. Doch warum hat er vorher von der Unterlegenheit gewisser Rassen geredet? Um in die Denkweise eines Menschen aus dem 19. Jahrhundert einzutauchen, sollte man sich die Ursprünge körperlichen Verfalls und von Krankheiten anzuschauen:

In seinem in der ersten Hälfte des 20. Jahrhunderts erschienen Buch "Nutrition and Physical Degeneration"<sup>[[4]](#fn-4)</sup> widmete sich der Zahnarzt Weston A. Price der Aufgabe, den Zusammenhang von Ernährung und Zahngesundheit (und Gesundheit allgemein) herauszufinden. Dazu bereiste er die ganze Welt und besuchte sog. primitiven Völker und Menschengruppen, die noch traditionell lebten, wie bspw. die Indianer in Amerika, die Aborigines in Australien, die Pygmäen in Afrika usw. Während dieser Untersuchungen machte er eine Reihe von Fotos zur Dokumentation. Hier exemplarisch einige Fotos von seinem Aufenthalt bei den Melanesiern (Die Bildunterschriften sind aus dem Buch entnommen):

<figure>
	<a href="https://files.sadir.me/posts/20210729-melanesier-fig-28.jpg">
		<img alt="Gesunde Malenasier" src="https://files.sadir.me/posts/20210729-melanesier-fig-28.jpg">
	</a>
	<figcaption>"These Melanesians are typical in general physical build and facial and dental arch form of their race which is spread over a wide area of Islands in the southeastern Pacific. The nutrition of all is adequate for them to develop and maintain their racial pattern."</figcaption>
</figure>

<figure>
	<a href="https://files.sadir.me/posts/20210729-melanesier-fig-29.jpg">
		<img alt="Gesunde Malenasier" src="https://files.sadir.me/posts/20210729-melanesier-fig-29.jpg">
	</a>
	<figcaption>"The development of the facial bones determines the size and shape of the palate and the size of the nasal air passages. Note the strength of the neck of the men above and the well proportioned faces of the girls below. Such faces are usually associated with properly proportioned bodies. Tooth decay is rare in these mouths so long as they use an adequate selection of the native foods."</figcaption>
</figure>

Im Vergleich zu diesen gesunden Melanesiern, die in Neukaledonien und den Fidschiinseln lebten und ihre traditionelle Ernährung aßen, fotografierte Price auch Melanesier, die deutliche körperliche Spuren zeigten von einer "modernen" Ernährung, die sie übernommen hatten:

<figure>
	<a href="https://files.sadir.me/posts/20210729-melanesier-fig-31.jpg">
		<img alt="Kranke Malenasier" src="https://files.sadir.me/posts/20210729-melanesier-fig-31.jpg">
	</a>
	<figcaption>"These natives of the Fiji Islands illustrate the effect of changing from the native food to the imported foods of commerce. Tooth decay becomes rampant and with it is lost the ability to properly masticate the food. Growing children and child bearing mothers suffer most severely from dental caries."</figcaption>
</figure>

<figure>
	<a href="https://files.sadir.me/posts/20210729-melanesier-fig-32.jpg">
		<img alt="Kranke Malenasier" src="https://files.sadir.me/posts/20210729-melanesier-fig-32.jpg">
	</a>
	<figcaption>"No dentists or physicians are available on most of these islands. Toothache is the only cause of suicide. The new generation born after the parents adopt the imported modern foods often have a change in the shape of the face and dental arches. The teeth are crowded as shown below."</figcaption>
</figure>

Weiter bemerkte Price, dass die Kinder, die geboren wurden nachdem die Eltern einer modernen Ernährung folgten, größere Fehlbildungen aufwiesen. Über die Maori schreibt er z.B.: "The reputation of the Maori people for splendid physiques has placed them on a pedestal of perfection. Much of this has been lost in modernization. However, through the assistance of the government, I was able to see many excellent physical specimens."

<figure>
	<a href="https://files.sadir.me/posts/20210729-maori-fig-69.jpg">
		<img alt="Gesunde Maori" src="https://files.sadir.me/posts/20210729-maori-fig-69.jpg">
	</a>
	<figcaption>"Since the discovery of New Zealand the primitive natives, the Mann, have had the reputation of having the finest teeth and finest bodies of any race in the world. These faces are typical. Only about one tooth per thousand teeth had been attacked by tooth decay before they came under the influence of the white man."</figcaption>
</figure>

<figure>
	<a href="https://files.sadir.me/posts/20210729-maori-fig-72.jpg">
		<img alt="Kranke Maori" src="https://files.sadir.me/posts/20210729-maori-fig-72.jpg">
	</a>
	<figcaption>"In striking contrast with the beautiful faces of the primitive Maori those born since the adoption of deficient modernized foods are grossly deformed. Note the marked underdevelopment of the facial bones, one of the results being narrowing of the dental arches with crowding of the teeth and an underdevelopment of the air passages. We have wrongly assigned these distorted forms to mixture of racial bloods."</figcaption>
</figure>

Das sind genau die körperlichen/biologischen Unterschiede, von denen Redbeard weiter oben geredet hat. Price hat eindeutig belegt, dass sie auf die Ernährung zurückzuführen und sie nichts der Rasse inhärentes sind. Wusste Redbeard es nicht? Hatte er nicht bedacht, dass es Menschen gibt, die nicht seiner Beschreibung entsprechen? Doch, in Kapitel 6.6 scheibt er:

> *A friend of Winwoode Reade tells a tale full of meaning. As an African explorer he once came across a native tribe (the Joloffs) remarkable for their comparative fine appearance. He asked one of them: "How is it that everyone whom I meet here is good-looking, not only your men but your women?" "That is easily explained", was the reply; "it has always been our custom to pick out our worst-looking ones, and sell them for slaves."*

Nun, haben die Joloffs die Wahrheit gesagt? Mag sein, mag aber auch nicht sein. Aajonus Vonderplanitz, der Begründer der Primal Diet, lebte selber viele Jahre mit verschiedenen Stämmen zusammen und laut seinen Informationen lügen viele Stämme Außenstehende (oder "den weißen Mann") an und erzählen erst dann die Wahrheit, wenn sie lange zusammengelebt und Vertrauen aufgebaut haben<sup>[[5]](#fn-5)</sup>:

<video controls src="https://files.sadir.me/posts/20210729-aajonus-vonderplanitz-staemme.webm"></video>

Redbeard war sich also dessen gewahr. Um zu verstehen, warum er sich dennoch so auf die Biologie fokussiert hat, muss man etwas tiefer eintauchen. In Kapitel 5.9 schreibt er:

> *Die Unterdrückung von einer Klasse durch eine andere wird stets verursacht durch die körperliche Feigheit der Opfer, und die Natur hat nichts übrig für Feiglinge – ob arm oder reich. Unterdrückung ist eine der *notwendigen* Phasen der Evolution. Um die Unterordnung und letztlich die Auslöschung der niederen Arten zu gewährleisten, ist der Kampf ums Überleben daher den Menschen ebenso auferlegt, wie allen anderen Tieren.*

> *Es ist die Aufgabe der Reichen, die Armen auszubeuten, und es ist ebenso die Aufgabe der Armen, sich im Gegenzug zu verteidigen und zurückzuschlagen.*

Wenn man besiegt und als Folge davon unterdrückt wird, bedeutet es für Redbeard, dass man feige war, denn die andere Art des Besiegtseins bedeutet, dass man für seine Freiheit gestorben ist. Entweder man triumphiert oder man stirbt als freien Mann. Das Dazwischen ist, was er veruteilt, denn dieser Zustand wird sich wie ein Fluch über dich und deine Nachkommen legen; s. Zitat oben: "Wenn er nicht getötet wird, müssen er und seine Nach­kom­men bis in die dritte oder vierte Ge­nera­ti­on in Un­ter­wer­fung leben".

In Unterwerfung leben bedeutet ein Leben von niederer Qualität zu haben. Es ist gemeinhin belegt, dass sich im Schnitt ärmere Menschen schlechter ernähren und dass sie eine ungesündere Lebensweise haben. Ein Gefangener bekommt nur Brot und Wasser; oder irgendeine unverdauliche Pampe. Leute, die aus Gefängnissen entlassen werden, zeigen deutliche Spuren der Alterung, da die vielen Jahre schlechter Ernährung, Einsamkeit, mangelnder Sonneneinstrahlung und frischer Luft und der Stress ihnen stark zugesetzt hat. Viele Menschen, allen voran aus unteren Klassen, leben auch in "Freiheit" nicht sehr viel anders, sie haben nur keine sichtbaren Gitter im Leben. Und auch Sklaven, die eins frei waren und dann zur Kriegsbeute wurden und in Unterdrückung leben mussten, werden von den Eroberern mit dem allermindesten abgespeist. Logisch, dass sich diese Menschen dann über Generationen zurückbilden und eine immer größere Differenz zwischen ihnen und ihren Herrschern entsteht. Das ist, was Redbeard beobachtet und beschrieben hat. Es entspricht nicht dem Rassismus, an den Menschen heutzutage denken müssen. In Kapitel 5.7 schreibt er:

> *Kein Mensch hat irgend ein *inhärentes* Recht auf die Nutzung der Erde, auf persönliche Unabhängigkeit, auf Besitz, auf Frauen, auf Redefreiheit, auf gedankliche Freiheit oder *irgend etwas* anderes, außer er kann seine "Rechte" – alleine oder in Verbindung mit seinen Verbündeten – aufgrund von Macht geltend machen. Was nach gängiger Redensart "Rechte" genannt wird, ist in Wahrheit "Beute" – das Vorrecht vorher ausgeübter Macht. Aber ein "Recht" schwindet sofort dahin, wenn diejenigen, die es genießen, unfähig werden es länger aufrechtzuerhalten. Folglich sind alle "Rechte" so vergänglich wie morgendliche Regenbögen, internationale Pakte oder zeitwilige Waffenstillstände. Sie können in jedem Moment von jeder der gegnerischen Parteien abgeschafft werden, welche die dazu erforderliche Macht besitzen.*

Weiter schreibt er im Unterkapitel "Philosophie der Macht":

> *Wie kann ein Sklave seine Freiheit wiedererlangen?*  
> *Indem er seinen Eroberer wiederum erobert. Wenn er fühlt, daß er das nicht schafft, *muß* er sich unterordnen, sich seine eigene Kehle aufschneiden oder ununterworfen kämpfend sterben.*

> *Du hast keinen Trost für die "Armen und Schwachen", die "Unschuldigen" und die "Niedergeworfenen"?*  
> *Die Armen und Schwachen sind eine schleichende Pest – es gibt keine Unschuldigen, und die Niedergeworfenen werden rechtmäßig verdammt – Sünder in einer Hölle, die sie selbst erschaffen haben.*  
> *Du preist die Starken und verherrlichst die Mächtigen?*  
> *Ja, das tue ich. Sie sind die Adligen der Natur. An ihnen erfreut sie sich, an denen, die alles Überwinden, den Furchtlosen.*

Man sieht deutlich, dass Redbeard nicht dogmatisch irgendeiner Rassenlehre folgt. Ihm geht es nur um den Stärkeren. Wer auch immer die Spitze in einem Kampf erklimmen konnte, ist für ihn der Überlegene, völlig unabhängig von der Hautfarbe. Wenn er doch von der Hautfarbe redet, geschieht es als Resultat einer geschichtlichen Wirklichkeit. Kämpfen bspw. zwei weiße Völker gegeneinander, wird er die Nachfahren der Besiegten ebenso als rassisch unterlegen bezeichnen, denn das ist das Resultat davon, dass die Besiegten schlechter leben müssen als die Eroberer.

Das ist, was Rechtsextremisten und Rassisten heutzutage nicht beachten. Sie glauben, die Farbe ihrer Haut macht sie überlegen und es gibt nichts, was das ändern könnte. So greifen sie ihre andersfarbigen Nachbarn und Mitmenschen an, in dem Glauben, dass diese ihnen unterlegen sind. Aber diese Leute unterscheiden sich von ihnen in keinster Weise, denn sie alle sind unter gleichen Umständen geboren und aufgewachsen, sie waren den gleichen Dingen ausgesetzt. Niemand von ihnen weist biologische Überlegenheit auf. Denn ob schwarz oder weiß oder etwas anderes, solange man ein "Normalbürger" ist, ist man ein Knecht. Sie alle sitzen im gleichen Boot und werden von Leuten regiert (ich spreche nicht von Politikern), die tatsächlich über viele Generationen die Möglichkeit hatten zu den Menschen in Knechtschaft eine Differenz aufzubauen, sodass sie sich wahrheitsgemäß als rassisch überlegen bezeichnen können. Es war ein Geniestreich dieser Leute den Begriff "Rassismus" überhaupt erst zu erschaffen, weil es nur dazu geführt hat, dass die Massen sich nur auf sich selbst fokussieren und sich gegenseitig zerfleischen, während die herrschende Klasse weiterhin in Ruhe von der Energie dieser Leute leben kann.

---
<ol class="fns">
	<li id="fn-1">Pseudonym, wahrscheinlich von Arthur Desmond</li>
    <li id="fn-2">Vom Verlag <a href="https://underworldamusements.com/collections/ua-books/products/might-is-right-ragnar-redbeard">Underworld Amusements</a></li>
    <li id="fn-3">Vom Verlag <a href="https://www.esoterick.de/">Edition Esoterick</a></li>
    <li id="fn-4">Gibt es auch kostenlos zu lesen auf der <a href="http://gutenberg.net.au/ebooks02/0200251h.html">Webseite der Project Gutenberg Australia</a> [<a href="https://www.sadir.me/archive/gutenberg.net.au/1650053353669/ebooks02/index.html">Archiv</a>]</li>
    <li id="fn-5"><a href="https://www.youtube.com/watch?v=yIm9ucmH2vc">Aajonus Talks about Weston Price</a></li>
</ol>
