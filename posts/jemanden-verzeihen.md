---
layout: post.njk
title: Jemanden verzeihen
date: 2023-01-30T20:26
---
Wenn jemand einen Neubeginn in seinem Leben hat, bspw. durch eine religiöse oder spirituelle Neuorientierung, neigt er oft dazu sein Leben Revue passieren zu lassen und mit alten Dingen aufzuräumen. Eines dieser Dinge ist es, mit den Konflikten mit Menschen aus der Vergangenheit abzuschließen. Das kann dann so aussehen, dass man mit ihnen wieder Kontakt aufnimmt und sich entweder entschuldigt oder generell das Gespräch sucht, wenn die andere Person diejenige ist, die einem Unrecht getan und sich zu entschuldigen hat. Oft überspringen Leute jedoch diesen Schritt und verzeihen der Person geistig und schließen mit diesem Akt dann das Kapital ab.

Das geht aber komplett entgegen der Natur einer Verzeihung. Man kann niemanden verzeihen, der sich nicht entschuldigt hat. Denn eine Entschuldigung bedeutet, dass man sich mit der vergangenen Tat nicht mehr identifiziert, dass man es nicht noch einmal tun würde. Die reumütige Person, die vor dir steht, ist also nicht mehr die gleiche, wie die Person, die dir Unrecht angetan hat. Es liegt nun an dir, ob du das akzeptierst.

Wenn sich die Person aber nie entschuldigt hat, gibt es keinen Unterschied zwischen ihr jetzt und damals. Vielleicht würde sie es heute genau so noch einmal tun. Was man also macht, während man einer Person verzeiht, die nie um Verzeihung gebeten hat, ist eine Fantasie-Version von ihr im Kopf zu kreieren, die sich von den Taten der realen Person distanziert und die sich bei einem entschuldigt. Die Person, der du verzeihst, existiert also nicht wirklich.

So wenig wie es Sinn ergibt, jemanden zu verzeihen, der einem gerade in dem Moment aktiv Leid antut, so wenig ergibt es auch Sinn bei jemanden, von dem man nicht weiß, ob er es nicht in dem Moment wieder tun würde.