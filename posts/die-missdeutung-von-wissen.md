---
layout: post.njk
title: Die Missdeutung von Wissen
date: 2020-08-17
---
Es entbehrt schon nicht einer gewissen Ironie, dass so viele Leute, die Wissenschaft romantisieren und sie für den Weisheit letzter Schluss halten, überhaupt nichts von dem Wesen der Wissenschaft verstehen. Ihre einzige Zugangsquelle zu der Thematik ist Populärwissenschaft. Aber Populärwissenschaft ist nur populär, weil sie keine Wissenschaft ist.

Vor allem sogenannte *science communicators* erwecken oft den Eindruck, dass Wissenschaft dazu diene, die Welt zu verstehen. Aufbauend auf diesem Irrtum blenden sie die Öffentlichkeit und verkaufen als Bildung verpackte Unterhaltung als den einzig richtigen und objektiven Weg die Welt wahrzunehmen. Man bekommt von klein auf gesagt, dass unsere Sinne uns täuschen und man zu irrigen Schlüssen kommen kann, wenn man sich auf sie verlässt.

Wissenschaft war aber niemals dazu gedacht, ein Mittel zu sein, die Welt zu verstehen. Sie wurde dazu nicht geschaffen und kann auch niemals diese Rolle erfüllen. Die heute weit verbreitete Vorstellung, dass Wissenschaft und Forschung die Gegenstücke zu Religion und Glaube sind, ist nur eine Illusion und eine relativ neue Modeerscheinung. Die klügsten Köpfe der vormodernen Zeiten wussten, dass Wissenschaft kein Zugang zum Verständnis der Welt war, sondern ein Mittel zur Erstellung von Modellen ebendieser.

Aufbauend auf diesen Modellen kann man entweder nützliche Werkzeuge erstellen oder Vorhersagen machen. Ein Modell ist aber per definitionem eine vereinfachte Form der Realität, nicht die Realität selbst. D.h. man soll sich nur insoweit darauf verlassen, wie es einem nützt, aber man soll nicht denken, dass man die Wirklichkeit dahinter versteht. **Man versteht das Modell und nicht die Sache, die das Modell abbildet.** Die vielen Variablen der wirklichen Sache, die im Modell aufgrund der Vereinfachung nicht enthalten sind, sind von entscheidender Bedeutung und Schlüsse, die man aufgrund der Modelle zieht, werden wegen der vernachlässtigen Variablen immer Nebeneffekte haben, mit denen man nicht rechnet.

> **All models are wrong, but some are useful.**  
> – George E. P. Box

Diese Methodik, mit Modellen zu arbeiten, statt mit der Wirklichkeit, ist nicht exklusiv wissenschaftlich, sondern die Grundlage der menschlichen Kommunikation und alter Weisheiten. Wenn man etwas erlebt und das Erlebte dann einem anderen weitererzählt, so bekommt der andere nur ein Abbild davon; also, das was wirklich passiert ist, abzüglich vieler Variablen, die im Spiel waren und du auch wahrgenommen hast, die aber beim Erzählen vernachlässigt werden.  
Das klassische Bild eines Schülers, der seinem Meister um Rat fragt und der Meister, statt direkt zu antworten, eine weise Anekdote erzählt, aus dem der Schüler selbst die Antwort herausfiltern soll, ist eine Demonstration für ein Modell für die Meinung des Meisters. Ebenso sind Sagen, Märchen und alte Erzählungen, die dem Leser oder der Leserin eine Lehre mitgeben sollen, eine Erscheinungsform derselben Sache – Die Geschichten sind Modelle der Botschaft.  
Auch diese Beispiele, die ich gerade gebe, sind nichts anderes als Modelle für das, was ich sagen will.

Nur nennt man sie außerhalb des wissenschaftlichen Kontextes nicht Modelle, sondern Beispiele und Analogien. Oder Geschichten und Anekdoten. Oder Sagen und Märchen. Je nachdem, in welchem Bereich man sich befindet, hat es eine andere Bezeichnung, aber es ist eine andere Form derselben Sache. **Was ein Modell für die Wissenschaft ist, ist eine Analogie für die Sprache.**

Die Denkweise der verschiedenen Modelle sind je nach Kategorie unterschiedlich – ob induktiv, deduktiv, syllogistisch etc. Ebenfalls sind die Ansprüche daran abhängig von der Kategorie. Während alte Erzählungen möglichst originaltreu und unverändert weitergegeben werden – heute in Form der Schrift, früher über tausende Jahre mündlich von Generation zu Generation – haben wissenschaftliche Modelle den Anspruch sich immer zu verändern. Die Veränderung ist Teil des Systems und wird Fortschritt genannt. Man darf aber nicht dem Irrtum erliegen, zu glauben, dass irgendwann die Reise ein Ende hat und man auf diese Weise tatsächlich die Wirklichkeit erkunden kann.

> **Wissenschaft ist Irrtum auf den letzten Stand gebracht.**  
> – Linus Pauling

Wem dem nicht so wäre, müssten Physiker die weisesten Menschen der Welt sein. Und ich denke nicht, dass mir irgendjemand widersprechen würde, wenn ich sage, dass es nicht weiter von der Realität entfernt sein könnte. Wissenschaftler bis ins 20. Jahrhundert hinein, die man tatsächlich als weise oder inspirierend betrachten könnte, hatten ihre Weisheit nicht durch die vernarrte Erhebung ihres Fachs, sondern durch ihr metaphysisches Verständnis und ihrer realistischen Vorstellung über die Rolle der Wissenschaft. Das sind Dinge, die um und nach dem 2. Weltkrieg leider weitesgehend verlorengegangen sind.
