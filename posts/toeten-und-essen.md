---
layout: post.njk
title: Töten und essen
date: 2021-08-31T20:40
---
Der ganz offensichtlich verrückte vegane Youtuber Vegan Gains hatte schon desöfteren in der Vergangenheit die Aussage gemacht, dass er es für sinnvoll erachte, karnivore Raubtiere wie Löwen und Tiger zu töten, da sie anderen Tieren schaden. Was auf dem ersten Blick wie ein Witz erscheint und auf dem zweiten Blick, wenn man realisiert, dass er es ernst meint, man als das Hinrgespinst eines Irregegangenen abtun kann, ist doch eine logische Konsequenz, wenn man bedenkt, was die Ursache dieser Haltung ist.

Für den Großteil der Menschheitsgeschichte war Fleisch das Hauptnahrungsmittel. Zu essen hieß, dass ein Tier habe sterben müssen. Wenn man selbst oder jemand aus dem Stamm jagen war und mit einem Tier zurückkam, hieß es, dass man seinen Hunger stillen konnte. War man nicht erfolgreich bei der Jagd, musste man hungern. Weit vor der Landwirtschaft und dem exzessiven Gebrauch von Feuer gab es nicht viele andere Optionen.

Das Essen, was offensichtlich ein menschlicher Trieb ist, da wir es instinktiv mit Überleben gleichstellen, konnte nicht vollzogen werden, ohne dass ein anderes Lebewesen dafür sterben musste. Essen war also synonym mit töten. Ist das Essen ein Trieb, so muss es das Töten auch sein, da sie miteinander verwoben sind.

Veganer versuchen nun die Verbindung zwischen essen und töten aufzulösen. Einigen mag das mehr oder weniger erfolgreich gelingen, doch es geht wie gesagt nur um die Auflösung der Verbindung. Veganismus kann lediglich nur versuchen die Assoziation von essen mit töten zu überwinden, er kann aber weder den Instinkt zu essen, noch den Instinkt zu töten zerstören.

So hat Vegan Gains tief drinnen das Gefühl, dass irgendetwas nicht richtig läuft, wenn kein Tier stirbt. Nur verbindet er dieses Gefühl nicht mit dem Trieb zu essen, weil seine vegane Philosophie diese aufgelöst hat. Um es aber einigermaßen erklären zu können, bringt er Moral ins Spiel und meint, dass es eine bessere Welt wäre, wenn kein Tier ein anderes essen würde und das sei Rechtfertigung genug, um zu versuchen die Regeln der Natur zu überschreiben. Jeder normale Mensch versteht natürlich wie absurd diese Aussage ist und das tut er selbst wahrscheinlich auch, wenn er einen Moment ehrlich zu sich ist, aber er kann diese Haltung nicht aufgeben, denn die einzige andere Erklärung für dieses Gefühl in ihm ist, dass leben und töten miteinander verkettet sind; und das würde sein ganzes veganes Kartenhaus zum Einsturz bringen.