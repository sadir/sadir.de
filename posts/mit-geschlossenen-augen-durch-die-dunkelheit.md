---
layout: post.njk
title: Mit geschlossenen Augen durch die Dunkelheit
date: 2020-08-05
---
Mir ist einmal aufgefallen, dass wenn ich durch einen dunklen Raum laufe – und damit meine ich durch einen komplett dunklen Raum, wo alles drumherum schwarz ist und ich nichts erkenne – dann fällt es mir sehr viel leichter mich zur orientieren und ich laufe schneller, wenn ich die Augen dabei schließe. Obwohl das Resultat der optischen Wahrnehmung für mich bei offenen und geschlossenen Augen exakt gleich ist.

Das fand ich recht interessant, anscheinend schaltet das Gehirn in einen anderen Modus, je nachdem, ob die Lider unten oder oben sind und nicht nach dem, was man sieht oder nicht sieht.

Nach kurzer Recherche bin ich überraschender Weise auch auf zwei wissenschaftliche Untersuchungen gestoßen, die sich genau damit befassen:

* [Walking with eyes closed is easier than walking with eyes open without visual cues: The Romberg task versus the goggle task](https://www.sciencedirect.com/science/article/pii/S1877065715005321?via%3Dihub)
* [Spatial orientation during eyes closed versus open in the dark: Are they the same?](https://www.scirp.org/html/10-1390042_19460.htm)

<p class="normal">Aber abgesehen davon konnte ich im Internet nicht unbedingt so viel zu dem Thema finden.</p>