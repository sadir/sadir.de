---
layout: post.njk
title: Nestlé, der Prügelknabe
date: 2022-01-18T13:20
---
Leute lieben es, auf [Nestlé herumzuhacken](https://www.youtube.com/watch?v=nvDpfnlo7wk). Das Unternehmen ist in den letzten Jahren zum Monsanto der Lebensmittelkonzerne avanciert und es gibt viele Menschen, die versuchen so gut wie möglich auf seine Produkte zu verzichten. Dieser Boykott ergibt aber nicht wirklich Sinn. Zwar ist Nestlé ein absoluter Müllkonzern, aber nichts was er macht ist ihm exklusiv.

Ist es besser auf Nestlé-Produkte zu verzichten, aber weiterhin die Produkte von Unilever, Procter & Gamble und co. zu konsumieren?

Ein Unternehmen, auch wenn er noch so groß ist, bestimmt nicht die Regeln des Spiels. Alles, was er macht, wird genausogut jedes andere große Unternehmen machen.

Abgesehen davon hat Nestlé kein einziges nicht toxisches Produkt im Sortiment, außer vielleicht das Wasser – aber selbst da ist davon auszugehen, dass es nicht von besonders guter Qualität ist. Wenn man schon einen Boykott macht, sollte man es nicht tun, weil man seine Machenschaften als unethisch erachtet – da auch jedes andere große Unternehmen genauso verfährt und man konsequenterweise alle boykottieren müsste – sondern weil der Konsum der Produkte gesundheitsschädlich ist. Wenn man so vorgeht, kann man problemlos die Produkte aller destruktiven Unternehmen boykottieren, da man nicht mehr auf Marken, sondern auf Inhalte schaut. 