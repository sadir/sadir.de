---
layout: post.njk
title: Fast Food
date: 2022-07-31T13:55
---
Die Tatsache, dass Fast Food eine negative Konnotation hat, weil man es mit ungesundem Fraß aus Schnellrestraurants assoziiert, impliziert die Schlussfolgerung, dass Mahlzeiten, die längere Zubereitungszeiten bedürfen, demnach besser und gesünder sind. Dies könnte aber nicht weiter von der Realität entfernt sein. Denn je länger der Kochprozess dauert, desto mehr entfernt man die Zutaten von ihrem natürlichen Zustand und desto fremder werden sie für unser Verdauungssystem.

In den meisten Fällen sind zu Hause zubereitete Mahlzeiten zwar gesünder, aber das liegt daran, dass sie von Grund auf gemacht werden, während in Schnellrestaurants bereits fertige Komponenten verwendet werden. Zieht man den ganzen Prozess in Betracht, inklusive der Von-Grund-Auf-Zubereitung der Komponenten an anderen Standorten, geht die Heimmahlzeit selbstverständlich schneller. Daher lässt sich die Daumenregel ableiten, dass je länger die Mahlzeit in Anspruch nimmt, desto mehr entfremdet man sie von ihrem natürlichen Zustand und desto ungesünder wird sie.

Demzufolge sollte der Begriff "Fast Food" eigentlich etwas Gutes sein, denn es ist ein Synonym für "(möglichst) unverarbeitet". Für die heute gängig als Fast Food bezeichneten Nahrungsmittel sollte nur noch ausschließlich das Synonym Junk Food verwendet werden.

Es gibt selbstverständlich auch Nahrungsmittel, die in ihrem rohen Zustand ungesünder sind als verarbeitet, aber das sollte man vielleicht als Zeichen nehmen, dass sie nicht für den menschlichen Verzehr gedacht sind.